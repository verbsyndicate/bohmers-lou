<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

 
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
// define('DB_NAME', 'digita26_client_database');
define('DB_NAME', 'bohmers');


/** MySQL database username */
// define('DB_USER', 'digita26_client');
define('DB_USER', 'root');

/** MySQL database password */
// define('DB_PASSWORD', 'cOa&Ic_$3H2X');
define('DB_PASSWORD', 'root');
// (+I#$oQyw0$7

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'E9$**%d#D1gTt=vMbnH!kMyQpI?c/T<Y]3xNtc=DqR@wP~d2}zY&81}[;FaOklBP');
define('SECURE_AUTH_KEY',  '7(~p]kisk3~u(fZc2R@D92e(H<UD/_))##MZ/L5[(pK4erk+%ZH-O8A0RX-8M@*J');
define('LOGGED_IN_KEY',    'EH8H=pY~(#a|x+C[p/owIZzwc-O]lG%!#yz#4Z^?21 7pc8d/P|Yny!Zp5kLnH_=');
define('NONCE_KEY',        '$XluVq-r&{*tcY3K|!/xSNm`b}_~UHpHTED`QBgp`+=>,0| }n;1i]o/AQs2UO:9');
define('AUTH_SALT',        'r-Zg:G^}NW_W2Vf<5dsz]/: ZPK,XMn3M0+:nsA=Fb.-J7%hC/.bx-$^|_ovnnz9');
define('SECURE_AUTH_SALT', '$nF&-6$iDnt`n&@r_9_$ l|kFm^:-+FwuaP,4ix`=^{AFJBulaU4T-hx^++dFUQ[');
define('LOGGED_IN_SALT',   '_.DALGj3d4I!csq`@Q%B_JJT?fic%q>5MrE^4=.7:.ezLM%qzjHa>~6F+Z$MA3#?');
define('NONCE_SALT',       '8 {?<u-!,fuz}Z|<Koa{8hZ19%AVg.O6;9mPmzejUm=Ai0g&q9}N%QbpIFU})O*w');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'btc__';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

// Disable XML-RPC
add_filter('xmlrpc_enabled', '__return_false');

// Disable post revisions
define('AUTOSAVE_INTERVAL', 300 ); // seconds
define('WP_POST_REVISIONS', false );
