<?php get_header(); ?>

<?php
    global $wp_query;
    $wp_query->query_vars['is_search'] = true;
    $s = str_replace("-", " ", $wp_query->query_vars['name']);
    $paged = get_query_var('paged') ? get_query_var('paged') : 1;
    $args = array (
        'post_type' => 'any',
        's' => $s,
        'paged' => $paged,
        'posts_per_page' => 10
    );
    $query = new WP_Query($args);
    $total = $query->found_posts;
?>

<main role="main" class="main">
    <div class="main__header">
        <div class="header__wrap container">
            <h1 class="main__title">Page not found</h1>
            <div class="main__subheading">
                <h3 class="main__subtitle">Sorry! We couldn't find what you were looking for.</h3>
            </div>
        </div>
    </div>
    <div class="main__body main__body--has-sidebar">
        <div class="container">
            <div class="main__content">
                <p>We had a look for you<?php echo $total > 1 ? ' and' : ', but'; ?> found <?php echo $total ?> possible result<?php echo $total === 1 ? '' : 's'; ?>. Maybe you could try searching again?</p>
                <?php get_template_part('components/search-form'); ?>

                <div class="loop__holder loop__holder--margin-top">
                    <?php
                         if($query) while ($query->have_posts()) : $query->the_post();
                               get_template_part('loop/item-list');
                        endwhile;
                    ?>
                    <div class="pagination">
                        <?php fx_pagination($query); ?>
                    </div>
                </div>
            </div>

            <aside class="main__sidebar">
                <?php get_sidebar(); ?>
            </aside>
        </div>
    </div>
</main>

<?php get_footer(); ?>
