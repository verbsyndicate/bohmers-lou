<?php if (!post_password_required()): ?>

<div class="post__comments">

    <h2 class="comments__title"><?php comments_number(); ?></h2>
    <?php if (have_comments()) : ?>

        <div class="comments__list">
            <?php wp_list_comments('type=comment&callback=fxcomments'); // Custom callback in functions.php ?>
        </div>

    <?php elseif ( ! comments_open() && ! is_page() && post_type_supports( get_post_type(), 'comments' ) ) : ?>

      <p>Comments are closed here</p>

    <?php endif; ?>

    <?php
    $comments_note = '<p class="comment__notes">Your email address will not be published</p>';

    $comments_field = '<div class="comments__form-row"><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true" placeholder="Your Comment"></textarea></div>';

    $fields =  array(
        'author' => '<div class="comments__form-row row"><div class="comments__input col-sm-4"><input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" placeholder="Your Name*"' . $aria_req . ' /></div>',
        'email' => '<div class="comments__input col-sm-4"><input id="email" name="email" type="text" value="' . esc_attr($commenter['comment_author_email']) . '" size="30" placeholder="Your Email*"' . $aria_req . ' /></div>',
        'url' => '<div class="comments__input col-sm-4"><input id="url" name="url" type="text" value="' . esc_attr($commenter['comment_author_url']) . '" size="30" placeholder="Your Website"/></div></div>',
    );

    $args = array(
    'class_submit' => 'btn btn--border',
    'comment_notes_before' => $comments_note,
    'comment_field' => $comments_field,
    'fields' => apply_filters('comment_form_default_fields', $fields)
    );
    comment_form($args);
    ?>
</div>

<?php endif; ?>
