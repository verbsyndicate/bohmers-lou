<div class="about__testimonials_wrapper">
  <div class="row">
    <div class="col-md-12">
      <h2>Testimonials</h2>
      <?php
      $all_testimonials = new WP_Query( array( 'post_type' => 'testimonial') );
      if ( $all_testimonials->have_posts() ) :
                    while ( $all_testimonials->have_posts() ) : $all_testimonials->the_post();
                      $testimonial_company = fx_get_meta('company');
          echo '<div class="about__testimonials_single-item"><span class="quote-mark">“ </span>';
          echo $testimonial_content = the_content();
          echo '<span class="quote-mark"> ”</span>';
          echo '<cite class="about__testimonial_cite">';
            echo $testimonial_company ;
          echo '</cite></div>';
          endwhile;
      endif;

      wp_reset_postdata();
      ?>

    </div>
  </div>
</div>
