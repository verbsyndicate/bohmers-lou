<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly
?>

<form action="#" method="post" name="contact-form" class="contact-form">

    <div class="form-group ignore">
        <input type="text" name="ignore" />
    </div>

    <div class="form-group">
        <label for="name" class="form__label">Name</label>
        <input type="text" name="name" id="name" class="form__input" required="" />
    </div>

    <div class="form-group">
        <label for="email" class="form__label">Email</label>
        <input type="email" name="email" id="email" class="form__input" required="" />
    </div>

    <div class="form-group">
        <label for="message" class="form__label">Message</label>
        <textarea name="message" id="message" rows="7" class="form__input" required=""></textarea>
    </div>

    <div class="form-group">
        <button type="submit" class="form__submit btn">Submit</button>
    </div>

    <div class="form__message"></div>
    <button type="button" class="form__repeat-button btn--icognito">Submit another message.</button>

</form>
