<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly
?>
<?php
global $gallery_id;
if (!$gallery_id) {
    return;
}

$args =  array (
    'p' => $gallery_id,
    'post_type' => 'gallery'
);
$query = new WP_Query($args);

if ($query) :
    while ($query->have_posts()) : $query->the_post();
        $images = json_decode(fx_get_meta('images', $gallery_id));
        if (count($images) === 0 ) {
            return;
        }
?>
<div class="gallery-holder">
    <div class="content__header">
        <h2 class="content__title"><?php the_title(); ?></h2>
    </div>
    <div class="gallery__content"><?php the_content(); ?></div>
    <div class="gallery__image-holder">
        <?php
        foreach ($images as $image) {
            echo '
                <div class="gallery__image" data-image="' . $image->url . '">
                    <div class="gallery__image-inner" style="background-image: url(' . $image->url . ');"></div>
                </div>';
        }
        ?>
    </div>
</div>
<?php
    endwhile;
endif;

wp_reset_postdata();
