<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly
?>

<?php
$item = '
<div class="home-clients__item">
    <img src="%1$s" alt="%2$s" />
</div>
';
?>

<div class="home-clients home-panel home-panel--orange">
    <h3 class="title title--border">OUR CUSTOMERS</h3>
    <div class="container container--wide">
        <div class="row row--no-gutter">
        <?php
        $args = array(
            'post_type' => 'partner',
            'posts_per_page' => 6,
            'order' => 'ASC'
        );

        $query = new WP_Query($args);

        if ($query) :
            while ($query->have_posts()) : $query->the_post();

                $title = get_the_title();
                $image = fx_post_image_url();

                printf($item, $image, $title);

            endwhile;
        endif;

        wp_reset_postdata();
        ?>
        </div>
    </div>
</div>
