<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly
?>

<div class="home-map home-panel">
    <h3 class="title title--border">Where we work</h3>
    <div id="location-map" class="map"></div>
</div>
