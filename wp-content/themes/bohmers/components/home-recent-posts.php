<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly
?>

<div class="home-recent-posts home-panel home-panel--grey">
    <h3 class="title title--border">Bohmer's Blog</h3>
    <div class="container">
        <div class="row">
        <?php
        $args = array(
            'post_type' => 'post',
            'posts_per_page' => 3
        );

        $query = new WP_Query($args);

        if ($query) :
            while ($query->have_posts()) : $query->the_post();

                echo '<div class="col-md-4">';
                get_template_part('loop/item-preview');
                echo '</div>';

            endwhile;
        endif;

        wp_reset_postdata();
        ?>
        </div>
    </div>
</div>
