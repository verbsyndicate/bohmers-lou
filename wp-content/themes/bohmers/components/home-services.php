<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly
?>

<?php
$item = '
<div class="col-md-4 home-services__item">
    <a href="%1$s" title="View %1$s">
        <div class="home-services__item__icon"><img src="%2$s" alt="%3$s" /></div>
        <h5 class="home-services__item__title">
            %3$s
        </h5>
        <span class="home-services__item__btn">Read more</span>
    </a>
</div>
';
?>

<div class="home-services home-panel">
    <h3 class="title title--border">Our Main Services</h3>
    <div class="container">
        <div class="row row--no-gutter">
        <?php
        $args = array(
            'post_type' => 'service',
            'posts_per_page' => 3,
            // 'order_by' => 'meta_value_num',
            'order' => 'ASC'
        );
        $query = new WP_Query($args);

        if ($query) :
            while ($query->have_posts()) : $query->the_post();

                $title = get_the_title();
                $permalink = get_permalink();
                $icon = fx_get_meta('icon');
                // $order = fx_get_meta('order');

                // echo $order;
                printf($item, $permalink, $icon, $title);
            endwhile;
        endif;

        wp_reset_postdata();
        ?>
        </div>
    </div>
    <div class="home-panel__buttons">
        <?php
        $contact_page = fx_get_url_from_select(get_opts('contact_page'));
        $service_page = fx_get_url_from_select(get_opts('service_page'));

        if ($service_page) {
            printf('<a href="%s" class="btn">Full services</a>', $service_page);
        }

        if ($contact_page) {
            printf('<a href="%s" class="btn">Contact Us</a>', $contact_page);
        }
        ?>
        <a href="<?php get_the_permalink();?>about/#about__testimonials_section" class="btn">See What Others Say</a>
    </div>
</div>
