<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly
?>

<div class="home-testimonials home-panel home-panel--transparent">
    <?php get_template_part('components/testimonial-carousel'); ?>
</div>
