<?php
$instagram_shortcode = get_opts('instagram_shortcode', true);
if ($instagram_shortcode) :
?>
<div class="instagram">
    <h3 class="title title--border">Instagram</h3>
    <div class="container">
        <?php echo do_shortcode($instagram_shortcode); ?>
    </div>
</div>
<?php endif; ?>
