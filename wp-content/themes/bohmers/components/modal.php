<div class="modal">
    <div class="container container--wide modal__container">
        <div class="modal__box">
            <div class="modal__inner"></div>
        </div>
    </div>
    <button type="button" class="modal__close"></button>
    <div class="modal__shadow"></div>
</div>
