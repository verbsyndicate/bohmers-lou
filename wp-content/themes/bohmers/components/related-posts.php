<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly
?>

<div class="related-posts">
    <h3 class="related-posts__title">Related News</h3>
    <div class="row">
    <?php
    $default_args = array(
        'post_type' => 'post',
        'posts_per_page' => 3
    );

    $post_type = get_post_type();

    if ($post_type === 'post') {
        $categories = get_the_category();
        $category = $categories[0]->slug;
        $custom_args = array(
            'category_name' => $category,
            'post__not_in' => array($post->ID)
        );
    }

    $args = $custom_args ? array_merge($default_args, $custom_args) : $default_args;

    $query = new WP_Query($args);

    if ($query) :
        while ($query->have_posts()) : $query->the_post();

            echo '<div class="col-md-4">';
            get_template_part('loop/item-preview');
            echo '</div>';

        endwhile;
    endif;

    wp_reset_postdata();
    ?>
    </div>
</div>
