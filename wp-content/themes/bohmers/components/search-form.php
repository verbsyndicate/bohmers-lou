<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly
?>

<form class="search-form" method="get" action="<?php echo home_url(); ?>" role="search">
    <input class="search-form__input" type="search" name="s" placeholder="Search">
    <button type="submit" class="search-form__submit icon--search"></button>
</form>
