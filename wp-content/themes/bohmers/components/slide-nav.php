<nav class="slide-nav">
    <a href="<?php echo get_site_url(); ?>" class="slide-nav__logo">
        <img src='<?php theme_dir(); ?>/assets/img/logo-inline-white.png' alt="Bohmer's Tree Care" />
    </a>
    <?php slide_nav(); ?>
    <?php
    $email = get_opts('email', true);
    if ($email) {
        printf('
        <div class="slide-nav__email">
            <span class="icon--envelope"></span>
            <a href="mailto:%1$s" title="Email us at %1$s">Email Us</a>
        </div>
        ', $email);
    }
    ?>
    <?php get_template_part('components/social-links'); ?>
</nav>
