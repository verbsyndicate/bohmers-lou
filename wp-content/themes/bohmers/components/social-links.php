<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly
?>

<?php
    $links = array(
        'twitter' => get_opts('social_twitter'),
        'facebook' => get_opts('social_facebook'),
        'google' => get_opts('social_google'),
        'tumblr' => get_opts('social_tumblr'),
        'instagram' => get_opts('social_instagram'),
        'linkedin' =>get_opts('social_linkedin'),
        'youtube' => get_opts('social_youtube')
    );
?>

<div class="social-links">
    <?php
    foreach ($links as $key => $link) {
        if (empty($link)) {
            continue;
        }
        $icon = 'icon--' . $key;
        echo '<a href="' . $link . '" target="_blank" class="' . $icon . '"></a>';
    }
    ?>
</div>
