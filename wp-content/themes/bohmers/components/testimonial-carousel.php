<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly
?>

<?php
$slide = '
  <div class="testimonial-carousel__slide swiper-slide">
    <a href="'.get_the_permalink().'about/#about__testimonials_section">
      <div class="slide__content">
          <blockquote class="slide__quote">
              <span class="quote-mark">&ldquo;</span>
              %s
              <span class="quote-mark">&rdquo;</span>
              <cite class="slide__cite">
                  %s<br />
                  %s
              </cite>
          </blockquote>
      </div>
  </a>
</div>
';

?>

<div class="swiper-container testimonial-carousel">
    <div class="swiper-wrapper testimonial-carousel__wrapper">
        <?php
        $args =  array(
            'post_type' => 'testimonial',
            'posts_per_page' => 5,
            'order' => 'desc',
            'orderby' => 'id',
        );
        $query = new WP_Query($args);

        if ($query) {
            while ($query->have_posts()) : $query->the_post();
                $title = get_the_title();
                $content = get_the_content();
                $company = fx_get_meta('company');

                printf($slide, $content, $title, $company);

            endwhile;
        }

        wp_reset_postdata();
        ?>
    </div>

    <div class="swiper-pagination"></div>
</div>
