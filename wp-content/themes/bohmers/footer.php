        <?php get_template_part('components/instagram'); ?>

        <footer class="footer">
            <div class="footer__body">
                <h3 class="title title--border">Contact Us</h3>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-6 col-lg-4">
                            <?php
                            $emergency_number = get_opts('emergency_number');
                            if ($emergency_number) {
                                printf('
                                <div>
                                    <span class="icon--phone"></span>Office: %s
                                </div>
                                ', $emergency_number);
                            }
                            ;?>
                        </div>
                        <div class="col-sm-12 col-md-6 col-lg-5 footer__social">
                            <?php
                            $email = get_opts('email');
                            if ($email) {
                                printf('
                                <div>
                                    <span class="icon--envelope"></span>%s
                                </div>
                                ', $email);
                            }

                            $social_links = array(
                                'facebook',
                                'instagram',
                            );

                            foreach ($social_links as $link) {
                                $opt_name = 'social_' . $link;
                                $link_url = get_opts($opt_name);

                                if (!$link_url) {
                                    return;
                                }

                                // Now build name
                                $link_name = get_opts($opt_name, true);
                                $link_name = $link === 'facebook' ?
                                    str_replace('https://facebook.com', '', $link_name) : "@$link_name";

                                printf('
                                <div>
                                    <span class="icon--%s"></span>
                                    <a href="%s">%s</a>
                                </div>
                                ', $link, $link_url, $link_name);
                            }
                            ?>
                        </div>
                        <div class="col-md-12 footer__badges">
                            <div class="footer__badges__holder">
                                <img src="<?php theme_dir(); ?>/assets/img/badge-1.png" alt="Badge 1" />
                                <img src="<?php theme_dir(); ?>/assets/img/badge-2.png" alt="Badge 2" />
                                <img src="<?php theme_dir(); ?>/assets/img/safework_awards_2016_winner.png" alt="SafeWork Award Winner" />
                                <img src="<?php theme_dir(); ?>/assets/img/2015_isc_lba.png" alt="Illawarra &amp; South Coast - Local Business Award Winner" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer__bottom">
                <p class="footer__copyright">
                    <?php get_template_part('components/verb-logo'); ?>
                </p>
            </div>
        </footer>

        <?php get_template_part('components/modal'); ?>

        <?php
        $analytics_code = get_opts('analytics_code');
        if ($analytics_code) :
        ?>
        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', '<?php echo $analytics_code; ?>', 'auto');
        ga('send', 'pageview');
        </script>
        <?php endif; ?>

        <?php wp_footer(); ?>

    </body>
</html>
