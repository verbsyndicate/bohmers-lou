<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

/*
 *  Author: Verb Syndicate
 *  URL: http://verbsyndicate.com
 */

/*------------------------------------*\
	External Traits/Classes
\*------------------------------------*/

define('GOOGLE_API_KEY', 'AIzaSyCe8GwbHjW0BXDOxKR7m2wxw3m3wv10L8U');
require_once('inc/fx-classes/functions.php');

/*------------------------------------*\
	External Function Modules/Files
\*------------------------------------*/

require_once('inc/fx-constants.php');
require_once('inc/fx-helpers.php');
require_once('inc/fx-functions.php');
require_once('inc/fx-filters.php');
require_once('inc/fx-admin.php');
require_once('inc/fx-cpt.php');
require_once('inc/fx-meta.php');
require_once('inc/fx-enqueue.php');
require_once('inc/fx-widgets.php');
require_once('inc/fx-taxonomies.php');
require_once('inc/fx-apperance.php');
require_once('inc/fx-theme.php');
require_once('inc/fx-api.php');
require_once('inc/fx-shortcodes.php');
require_once('inc/fx-settings.php');

/*------------------------------------*\
	Actions + Filters
\*------------------------------------*/

// Add Actions
add_action('init', 'fx_header_scripts'); // Add Custom Scripts to wp_head
add_action('wp_enqueue_scripts', 'fx_styles'); // Add Theme Stylesheet
add_action('init', 'register_fx_menu'); // Add Nav Menu
add_action('init', 'fx_pagination'); // Add our HTML5 Pagination
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
//add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

// Add Filters
add_filter('upload_mimes', 'wpcontent_svg_mime_type'); // Allow svg uploads
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'fx_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerp
