<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <?php
            $site_name = get_bloginfo('name');
            $page_title = wp_title('', false);
            $image_url = fx_post_image_url();
            $image_url = $image_url !== '' ? $image_url : get_template_directory_uri() . 'assets/img/default-feature.jpg';
            $meta_description = is_front_page() ? get_bloginfo() : strip_tags(fx_post_intro(2));
            $meta_title = $site_name . ($page_title ? ' - ' . $page_title : '');
        ?>
        <title>
            <?php echo $site_name . ($page_title ? ' | ' . $page_title : ''); ?>
        </title>

        <link href="//www.google-analytics.com" rel="dns-prefetch">
        <link href="<?php theme_dir(); ?>/assets/img/icons/favicon.ico" rel="shortcut icon">
        <link href="<?php theme_dir(); ?>/assets/img/icons/touch.png" rel="apple-touch-icon-precomposed">
        <link href="<?php theme_dir(); ?>/assets/img/icons/touch.png" rel="icon" sizes="192x192">

        <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, shrink-to-fit=no">

        <?php wp_head(); ?>

        <!--[if lt IE 10]>
            <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
            <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
        <![endif]-->

        <meta name="description" content="<?php echo $meta_description; ?>">
        <!-- Schema.org markup for Google+ -->
        <meta itemprop="name" content="<?php echo $meta_title; ?>">
        <meta itemprop="description" content="<?php echo $meta_description; ?>">
        <meta itemprop="image" content="<?php echo $image_url; ?>">
        <!-- Open Graph data -->
        <meta property="og:title" content="<?php echo $meta_title; ?>" />
        <meta property="og:type" content="article" />
        <meta property="og:url" content="<?php echo get_permalink(); ?>" />
        <meta property="og:image" content="<?php echo $image_url; ?>" />
        <meta property="og:description" content="<?php echo $meta_description; ?>" />
        <meta property="og:site_name" content="" />
    </head>
    <body <?php body_class(); ?>>
        <header class="header" role="banner">
            <a href="<?php echo get_site_url(); ?>" class="header__logo">
                <img src='<?php theme_dir(); ?>/assets/img/logo-inline.png' alt="Bohmer's Tree Care" />
            </a>
            <div class="mobile__emergency-button">
              <?php
              $emergency_number = get_opts('emergency_number');

              if ($emergency_number) {
                  printf('
                  <a href="tel:%s" class="header__callout">
                      <span>
                          <strong>Is it an emergency?</strong>
                          24/7 Callout service
                      </span>
                  </a>', $emergency_number);
              }
              ?>
            </div>
            <div class="header__body">
                <nav class="header__nav" role="navigation">
                    <?php main_nav(); ?>
                </nav>
                <button type="button" class="nav-icon nav-trigger"><div></div></button>
                <button type="button" class="search-trigger header__search-trigger">
                    <span class="icon--search"></span>
                </button>
                <?php
                $emergency_number = get_opts('emergency_number');

                if ($emergency_number) {
                    printf('
                    <a href="tel:%s" class="header__callout">
                        <span>
                            <strong>Is it an emergency?</strong>
                            24/7 Callout service
                        </span>
                    </a>', $emergency_number);
                }
                ?>
            </div>
        </header>
        <div class="search-dropdown">
            <?php get_template_part('components/search-form'); ?>
        </div>
        <?php get_template_part('components/slide-nav'); ?>
        <div class="header-clearfix"></div>
