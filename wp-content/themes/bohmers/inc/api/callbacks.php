<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

function fx_api_form_post($data) {
    $form = $data['slug'];
    $form_data = $_POST;
    $token = $form_data['token'];

    global $verified_nonce;

    if (!isset($token) || !$verified_nonce) {
        $response_data = array(
            'success' => false,
            'message' => 'Invalid form nonce.',
            'token' => $token,
        );
    } else {

        $form_fields = get_form_fields($form);

        $process_form = new ProcessForm($form, $form_fields);

        // Get options from admin
        $to = get_opts('form_email');
        if ($to) {
            $process_form->set_options(array(
                'to' => array($to),
                'subject' => 'Website Contact Submission'
            ));
        }

        // All good to submit
        $process_form->submit($form_data);

        // Get response
        $response_data = $process_form->response();
    }

    return fx_api_response($response_data);
}
