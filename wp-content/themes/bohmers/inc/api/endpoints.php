<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

add_action( 'rest_api_init', 'fx_register_api_hooks' );
function fx_register_api_hooks() {
    $namespace = API_BASE . API_VERSION;
    // Submit form
    register_rest_route($namespace, '/form/(?P<slug>[a-zA-Z0-9-]+)', array(
        'methods'  => 'POST',
        'callback' => 'fx_api_form_post',
    ));

}

// Needed to verify nonces in callbacks
add_action('rest_api_init', 'global_verified_nonce');
function global_verified_nonce() {
    // Setup global variable
    global $verified_nonce;
    $verified_nonce = false;

    // Get token
    $token = $_POST['token'];
    if ($token && wp_verify_nonce($token, API_NONCE)) {
        $verified_nonce = true;
    }
}
