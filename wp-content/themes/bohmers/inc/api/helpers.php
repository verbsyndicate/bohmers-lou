<?php

function sort_by_id($a, $b) {
    return $a['id'] - $b['id'];
}

function get_form_fields($form) {
    if ($form === 'contact') {
        return array(
            array('name', 'string', true),
            array('email', 'email', true),
            array('message', 'string', true),
        );
    }

    if ($form === 'enquiry') {
        return array(
            array('name', 'string', true),
            array('email', 'email', true),
        );
    }

    return null;
}
