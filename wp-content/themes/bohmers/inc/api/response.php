<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

function fx_api_response($input) {
    // Headers set in .htaccess
    $response = new WP_REST_Response($input);

    return $response;
}
