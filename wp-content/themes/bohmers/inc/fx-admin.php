<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

class ThemeOptions extends AdminPages {

    public $options_name = 'theme_options';
    public $base_name = 'theme-setting-admin';

    public function __construct() {
        parent::__construct();
    }

    public function register_admin_page() {
        add_menu_page(
            'Site Details',
            'Site Details',
            'manage_options',
            $this->base_name,
            array($this, 'create_admin_page'),
            'dashicons-admin-generic'
        );
    }

    public function create_admin_page() {
        // Enqueue media lightbox
        wp_enqueue_media();

        // Get options
        $this->options = get_option($this->options_name);

        ?>
        <div class="fx-admin-page wrap">
            <h1><?php bloginfo(); ?> Details</h1>
            <form method="post" action="options.php">
            <?php
                settings_fields($this->options_group);
                do_settings_sections('theme-setting-admin');
                submit_button();
            ?>
            </form>
        </div>
        <?php
    }

    public function page_init() {

        $contact_section = array(
            'id' => 'contact_section_id',
            'title' => 'Contact Details',
            'description' => '',
            'fields' => array(
                'phone_numbers' => array(
                    'type' => 'input-pairs',
                    'title' => 'Phone Number',
                    'description' =>  '',
                    'sanitize' => true
                ),
                'email' => array(
                    'type' => 'textfield',
                    'title' => 'Email',
                    'description' =>  '',
                    'sanitize' => true
                ),
                'emergency_number' => array(
                    'type' => 'textfield',
                    'title' => 'Emergency Callout Number',
                    'description' =>  '',
                    'sanitize' => true
                ),
            )
        );

        $social_section = array(
            'id' => 'social_section_id',
            'title' => 'Social Media Details',
            'description' => '',
            'fields' => array(
                'social_facebook' => array(
                    'type' => 'textfield',
                    'title' => 'Facebook URL',
                    'description' =>  '(Please include https://facebook.com)',
                    'sanitize' => true
                ),
                'social_google' => array(
                    'type' => 'textfield',
                    'title' => 'Google+ URL',
                    'description' =>  '(http://)',
                    'sanitize' => true
                ),
                'social_twitter' => array(
                    'type' => 'textfield',
                    'title' => 'Twitter Username',
                    'description' =>  '',
                    'sanitize' => true
                ),
                'social_instagram' => array(
                    'type' => 'textfield',
                    'title' => 'Instagram Username',
                    'description' =>  '',
                    'sanitize' => true
                ),
                'social_linkedin' => array(
                    'type' => 'textfield',
                    'title' => 'LinkedIn URL',
                    'description' =>  '(http://)',
                    'sanitize' => true
                ),
                'social_youtube' => array(
                    'type' => 'textfield',
                    'title' => 'Youtube URL',
                    'description' =>  '(http://)',
                    'sanitize' => true
                )
            )
        );

        $analytics_section = array(
            'id' => 'analytics_section_id',
            'title' => 'Analytics Details',
            'description' => '',
            'fields' => array(
                'analytics_code' => array(
                    'type' => 'textfield',
                    'title' => 'Analytics Tracking Code',
                    'description' =>  'UA-XXXXXXXX-X',
                    'sanitize' => true
                )
            )
        );

        $page_section = array(
            'id' => 'page_section_id',
            'title' => 'Site Options',
            'description' => '',
            'fields' => array(
                'instagram_shortcode' => array(
                    'type' => 'textfield',
                    'title' => 'Instagram shortcode',
                    'description' =>  'Enter the shortcode to display the instagram feed',
                    'sanitize' => false
                ),
                'contact_page' => array(
                    'type' => 'select',
                    'options' => 'page',
                    'title' => 'Contact Page',
                    'description' =>  '',
                    'sanitize' => true
                ),
                'service_page' => array(
                    'type' => 'select',
                    'options' => 'page',
                    'title' => 'Services Page',
                    'description' =>  '',
                    'sanitize' => true
                )
            )
        );

        $sections = array(
            $contact_section,
            $social_section,
            $analytics_section,
            $page_section,
        );

        $this->register_options_page('theme-setting-admin', $sections);
    }
}

if (is_admin()) {
    $my_settings_page = new ThemeOptions();
}
