<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

/*------------------------------------*\
    WP API Endpoints
\*------------------------------------*/

register_activation_hook( __FILE__, 'fx_api_activate' );
function fx_api_activate() {
    flush_rewrite_rules();
}

require_once 'api/helpers.php';
require_once 'api/endpoints.php';
require_once 'api/callbacks.php';
require_once 'api/response.php';

/*------------------------------------*\
    WP API Headers
\*------------------------------------*/

add_action('rest_api_init', function() {
    remove_filter('rest_pre_serve_request', 'rest_send_cors_headers');
    add_filter('rest_pre_serve_request', function($value) {
        header('Access-Control-Allow-Origin: *'); // Consider restricting for prodution
        header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
        header('Access-Control-Allow-Credentials: true');

        return $value;
    });
}, 15);
