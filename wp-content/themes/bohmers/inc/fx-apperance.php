<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

/*------------------------------------*\
    Register Menus
\*------------------------------------*/

function register_fx_menu() {
    register_nav_menus(array(
        'nav-menu' => 'Navigation Menu',
    ));

    register_nav_menus(array(
        'slide-menu' => 'Slide Menu',
    ));
}

/*------------------------------------*\
    Navigation
\*------------------------------------*/

function main_nav() {
    wp_nav_menu(
    array(
        'theme_location' => 'nav-menu',
        'menu' => '',
        'container' => false,
        'container_class' => '',
        'container_id' => '',
        'menu_class' => 'menu',
        'menu_id' => '',
        'echo' => true,
        'fallback_cb' => 'wp_page_menu',
        'before' => '',
        'after' => '',
        'link_before' => '',
        'link_after' => '',
        'items_wrap' => '<ul>%3$s</ul>',
        'depth' => 0,
        'walker' =>  '',
    ));
}

function slide_nav() {
    wp_nav_menu(
    array(
        'theme_location' => 'slide-menu',
        'menu' => '',
        'container' => false,
        'container_class' => '',
        'container_id' => '',
        'menu_class' => 'menu',
        'menu_id' => '',
        'echo' => true,
        'fallback_cb' => 'wp_page_menu',
        'before' => '',
        'after' => '',
        'link_before' => '',
        'link_after' => '',
        'items_wrap' => '<ul>%3$s</ul>',
        'depth' => 0,
        'walker' =>  '',
    ));
}

/*------------------------------------*\
    Widget Areas
\*------------------------------------*/

if (function_exists('register_sidebar')) {
    /* Sidebar */
    register_sidebar(array(
        'name' => 'Default Sidebar',
        'description' => 'Default sidebar for all pages',
        'id' => 'default-sidebar',
        'before_widget' => '<div id="%1$s" class="%2$s widget__holder">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget__title">',
        'after_title' => '</h3>'
    ));
}
