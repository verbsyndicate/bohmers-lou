(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
(function (global){
'use strict';

/* eslint-disable no-unused-vars */
window.$ = window.jQuery = (typeof window !== "undefined" ? window['jQuery'] : typeof global !== "undefined" ? global['jQuery'] : null);

require('./partials/iconPicker');

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"./partials/iconPicker":2}],2:[function(require,module,exports){
'use strict';

/* eslint-disable */
(function ($) {
    $.fn.iconPicker = function (options) {
        if (!options || !options.icons) {
            console.warn('No icons');
            return;
        }

        var icons = options.icons;
        var $button = $('.icon-picker');

        $button.each(function () {
            createPopup($(this));

            $(this).on('click.iconPicker', function () {
                showPopup($(this));
            });
        });

        function build_list($popup, $button) {
            var $list = $popup.find('.icon-picker-list');

            // Get input to save value
            var targetSelector = $button.data('target');
            var $target = $(targetSelector);

            for (var i in icons) {
                $list.append('<li data-icon="' + icons[i] + '"><a href="#" class="icon-picker-option" title="' + icons[i] + '"><span class="icon--' + icons[i] + '"></span></a></li>');
            };

            $('.icon-picker-option', $list).click(function (e) {
                e.preventDefault();
                var title = $(this).attr('title');
                var iconClass = title ? 'icon--' + title : '';
                $target.val(title);
                $button.removeClass().addClass('icon-picker ' + iconClass);

                if (title) {
                    removePopup($button);
                }
            });
        };

        function showPopup($button) {
            var $parent = $button.parent('.select-icon');
            $parent.find('.icon-picker-container').removeClass('hidden');
        }

        function removePopup($button) {
            var $parent = $button.parent('.select-icon');
            $parent.find('.icon-picker-container').addClass('hidden');
        }

        function createPopup($button) {
            var $parent = $button.parent('.select-icon');
            var targetSelector = $button.data('target');
            var $target = $(targetSelector);
            var $popup = $('<div class="icon-picker-container hidden"> \
                    <div class="icon-picker-control" /> \
                    <ul class="icon-picker-list" /> \
                </div>');

            build_list($popup, $button);

            var $control = $popup.find('.icon-picker-control');
            $control.html('<a data-direction="back" href="#"><span class="dashicons dashicons-arrow-left-alt2"></span></a> ' + '<input type="text" class="" placeholder="Search" />' + '<a data-direction="forward" href="#"><span class="dashicons dashicons-arrow-right-alt2"></span></a>' + '');

            var $list = $popup.find('.icon-picker-list');

            $('a', $control).click(function (e) {
                e.preventDefault();
                if ($(this).data('direction') === 'back') {
                    // move last 25 elements to front
                    $('li:gt(' + (icons.length - 26) + ')', $list).each(function () {
                        $(this).prependTo($list);
                    });
                } else {
                    // move first 25 elements to the end
                    $('li:lt(25)', $list).each(function () {
                        $(this).appendTo($list);
                    });
                }
            });

            $popup.appendTo($parent);

            $('input', $control).on('keyup', function (e) {
                var search = $(this).val();
                if (search === '') {
                    //show all again
                    $('li:lt(25)', $list).show();
                } else {
                    $('li', $list).each(function () {
                        if ($(this).data('icon').toString().toLowerCase().indexOf(search.toLowerCase()) !== -1) {
                            $(this).show();
                        } else {
                            $(this).hide();
                        }
                    });
                }
            });

            $(document).mouseup(function (e) {
                e.stopPropagation();
                if (!$popup.hasClass('hidden') && !$popup.is(e.target) && $($popup).has(e.target).length === 0) {
                    removePopup($button);
                }
            });
        }
    };
})(jQuery);

},{}]},{},[1]);
