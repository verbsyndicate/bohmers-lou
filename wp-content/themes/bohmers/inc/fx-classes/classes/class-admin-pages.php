<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

abstract class AdminPages {

    use MetaBoxes;

    protected $options;
    protected $fields = array();

    protected $options_name;

    public function __construct() {
        add_action('admin_menu', array($this, 'register_admin_page'));
        add_action('admin_init', array($this, 'page_init'));

        $this->options_page = $this->options_name;
        $this->options_group = $this->options_name . '_group';
    }

    abstract public function register_admin_page();

    abstract public function page_init();

    public function register_options_page($page, $sections) {
        register_setting(
            $this->options_group,
            $this->options_name,
            array($this, 'sanitize_input')
        );

        $this->register_field_sections($page, $sections);
    }

    public function register_field_sections($page, $sections) {
        foreach ($sections as $section) {
            $section_id = $section['id'];
            $title = $section['title'];
            $section_fields = $section['fields'];

            add_settings_section(
                $section_id,
                $title,
                array($this, 'render_section_info'),
                $page
            );

            $this->fields = array_merge($this->fields, $section_fields);

            foreach ($section_fields as $field_id => $field) {

                $args = array(
                    'id' => $field_id,
                    'type' => $field['type'],
                    'options' => $field['options'],
                    'n_options' => $field['n_options'],
                    'description' => $field['description']
                );

                $callback = array($this, 'render_input_fields');

                add_settings_field(
                    $field_id,
                    $field['title'],
                    $callback,
                    $page,
                    $section_id,
                    $args
                );
            }
        }
    }

    public function sanitize_input($input) {
        $new_input = array();
        $fields = $this->fields;

        foreach ($fields as $id => $field) {
            $sanitize = $field['sanitize'];

            if (!isset($input[$id])) {
                continue;
            }

            if ($sanitize) {
                $new_input[$id] = sanitize_text_field($input[$id]);
            } else {
                $new_input[$id] = $input[$id];
            }

        }

        return $new_input;
    }

    public function render_section_info($info) {
        echo '';
    }

    public function render_input_fields($args) {
        $id = $args['id'];
        $input_id = $this->options_name . $id;

        $button_text = $args['button_text'];
        $class = $args['class'];
        $description = $args['description'];
        $exclude_empty_option = $args['exclude_empty_option'];
        $input_type = $args['input_type'];
        $label = $args['label'];
        $n_options = $args['n_options'];
        $name = $this->options_name . "[$id]";
        $options = MetaHelpers::get_meta_options($args['options']);
        $ref = $this->options_name . "[$id]";
        $type = $args['type'];
        $value = $this->options[$id];
        $value_escaped = is_string($value) ? htmlspecialchars($value) : $value;

        $input_args = array(
            'button_text' => $button_text,
            'class' => $class,
            'description' => $description,
            'exclude_empty_option' => $exclude_empty_option,
            'id' => $input_id,
            'input_type' => $input_type,
            'label' => $label,
            'n_options' => $n_options,
            'name' => $name,
            'options' => $options,
            'ref' => $ref,
            'type' => $type,
            'value_escaped' => $value_escaped,
            'value' => $value
        );

        MetaBoxes::render_input($input_args);
    }
}
