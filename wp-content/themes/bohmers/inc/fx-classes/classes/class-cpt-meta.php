<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

class CustomPostMeta {

    use MetaBoxes;

    static $prefix = '_fx_';

    public function __construct($id, $options, $meta_boxes, $post_type) {
        $this->id = $id;
        $this->post_type = $post_type;
        $this->options = $options;

        // Need to check if meta section is laid out in columns
        $has_columns = isset($this->options['columns']) && $this->options['columns'];

        if ($has_columns) {
            // Save meta box layout to object
            $this->columns = $meta_boxes;
            // Setup empty meta box array
            $this->meta_boxes = array();
            // Need to concat all column inputs into one large array for saving
            foreach ($this->columns as $column) {
                $this->meta_boxes = array_merge($this->meta_boxes, $column['inputs']);
            }
        } else {
            // No columns, just set meta boxes normally
            $this->meta_boxes = $meta_boxes;
        }

        $this->add_actions();
    }

    private function add_actions() {
        add_action('admin_menu', array($this, 'add_meta_box'));
        add_action('save_post', array($this, 'save_postdata'));
    }

    public function save_postdata($post_id) {
        global $post, $meta_box_groups;

        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) {
            return $post_id;
        }

        if (defined('DOING_AJAX') && DOING_AJAX ) {
            //Prevents the metaboxes from being overwritten while quick editing.
            return $post_id;
        }

        if (preg_match('/\edit\.php/', $_SERVER['REQUEST_URI']) ) {
            //Detects if the save action is coming from a quick edit/batch edit.
            return $post_id;
        }

        foreach ($this->meta_boxes as $slug => $meta_box) {
            $name = $this::$prefix . $slug;
            $ref = $name . '_value';

            // Check for readonly
            if (isset($meta_box['readonly']) && $meta_box['readonly'] === true) {
                return null;
            }

            // Verify
            if (isset($_POST[$name . '_noncename'])) {
                if ( !wp_verify_nonce( $_POST[$name . '_noncename'])) {
                    return $post_id;
                }
            }

            if (isset($_POST['post_type']) && 'page' == $_POST['post_type']) {
                if (!current_user_can('edit_page', $post_id)) {
                    return $post_id;
                }
            } else {
                if (!current_user_can( 'edit_post', $post_id)) {
                    return $post_id;
                }
            }

            $data = '';
            if (isset($_POST[$ref])) {
                $data = $_POST[$ref];
            }

            if (get_post_meta($post_id, $ref) === '') {
                add_post_meta($post_id, $ref, $data, true);
            } elseif ($data != get_post_meta($post_id, $ref, true)) {
                update_post_meta($post_id, $ref, $data);
            } elseif ($data === '') {
                delete_post_meta($post_id, $ref, get_post_meta($post_id, $ref, true));
            }
        }
    }

    public function add_meta_box() {
        // Check input contains meta array
        if (!function_exists('add_meta_box') || !is_array($this->meta_boxes)) {
            return;
        }

        if ((isset($this->options['prerequisite']) && $this->options['prerequisite'])
        || !isset($this->options['prerequisite'])
        ) {
            add_meta_box(
                $this::$prefix . 'meta_' . $this->id,
                $this->options['title'],
                array($this, 'render_meta_box_group'),
                $this->post_type,
                $this->options['position'],
                $this->options['priority'],
                array(
                    'inputs' => $this->meta_boxes
                )
            );
        }
    }

    public function render_meta_box_group($post, $metabox) {
        // Check from constructor on how to render meta boxes
        $has_columns = is_array($this->columns);

        if ($has_columns) {
            echo '<div class="row">';

            foreach ($this->columns as $column) {
                echo '<div class="' . $column['class'] . '">';
                $this->render_meta_boxes($column['inputs']);
                echo '</div>';
            }

            echo '</div>';
        } else {
            // No colums, render normally
            $this->render_meta_boxes($this->meta_boxes);
        }

        echo '<br style="clear:both" />';

    }

    private function render_meta_boxes($meta_boxes_inputs) {
        if (!is_array($meta_boxes_inputs)) {
            return;
        }

        global $post;

        foreach ($meta_boxes_inputs as $slug => $meta_box) {
            $name = $this::$prefix . $slug;
            $ref = $name . '_value';

            $button_text = $meta_box['button_text'];
            $class = $meta_box['class'];
            $description = $meta_box['description'];
            $exclude_empty_option = $meta_box['exclude_empty_option'];
            $input_type = $meta_box['input_type'];
            $label = $meta_box['label'];
            $n_options = $meta_box['n_options'];
            $options = MetaHelpers::get_meta_options($meta_box['options']);
            $title = $meta_box['title'];
            $type = $meta_box['type'];
            $value_as_object = $meta_box['value_as_object'];

            $value = get_post_meta($post->ID, $ref, true);
            if ($value === '') {
                $value = isset($meta_box['default']) ? $meta_box['default'] : '';
            }

            $value_escaped = is_string($value) ? htmlspecialchars($value) : $value;

            $input_args = array(
                'button_text' => $button_text,
                'class' => $class,
                'description' => $description,
                'exclude_empty_option' => $exclude_empty_option,
                'id' => $name,
                'input_type' => $input_type,
                'label' => $label,
                'n_options' => $n_options,
                'name' => $name,
                'options' => $options,
                'ref' => $ref,
                'title' => $title,
                'type' => $type,
                'value' => $value,
                'value_as_object' => $value_as_object,
                'value_escaped' => $value_escaped,
            );

            MetaBoxes::render_input($input_args);
        }
    }
}
