<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

abstract class CustomWidget extends WP_Widget {

    use MetaBoxes;

    static $widget_id = '';
    static $options = array();
    static $fields = array();

    function __construct() {
        if (!$this::$widget_id || empty($this::$options) || empty($this::$fields)) {
            return;
        }

        $this->domain = $this::$widget_id . '_domain';
        $this->fields = $this::$fields;

        $title = $this::$options['title'];
        $description = $this::$options['description'];

        WP_Widget::__construct(
            $this::$widget_id,
            __($title, $this->domain),
            array(
                'description' => __($description, $this->domain),
                'classname' => 'widget--' . $this::$widget_id
            )
        );
    }


    // You must create this function to render the widget
    // public function widget($args, $instance);

    public function form($instance) {
        if ($this->fields) :
            foreach ($this->fields as $slug => $options) {
                $this->render_input_field($instance, $slug, $options);
            }
        endif;
    }

    private function render_input_field($instance, $slug, $args) {
        $id = $this->get_field_id($slug);
        $name = $this->get_field_name($slug);

        // Input options
        $button_text = $args['button_text'];
        $class = $args['class'];
        $description = $args['description'];
        $exclude_empty_option = $args['exclude_empty_option'];
        $input_type = $args['input_type'];
        $label = $args['label'];
        $n_options = $args['n_options'];
        $options = MetaHelpers::get_meta_options($args['options']);
        $title = $args['title'];
        $type = $args['type'];

        // Value
        $default = isset($args['default']) ? $args['default'] : '';
        $value = isset($instance[$slug]) ? $instance[$slug] : $default;
        $value_escaped = is_string($value) ? htmlspecialchars($value) : $value;

        $input_args = array(
            'button_text' => $button_text,
            'class' => $class,
            'description' => $description,
            'exclude_empty_option' => $exclude_empty_option,
            'id' => $id,
            'input_type' => $input_type,
            'label' => $label,
            'n_options' => $n_options,
            'name' => $name,
            'options' => $options,
            'ref' => $name,
            'title' => $title,
            'type' => $type,
            'value_escaped' => $value_escaped,
            'value' => $value
        );

        MetaBoxes::render_input($input_args);
    }

    public function update($new_instance, $old_instance) {
        $instance = array();
        foreach ($this->fields as $slug => $options) {
            $instance[$slug] = (!empty($new_instance[$slug])) ? strip_tags($new_instance[$slug]) : '';

            // Convert checkbox to boolean
            if ($options['type'] === 'checkbox') {
                $instance[$slug] = $instance[$slug] === 'true';
            }

        }
        return $instance;
    }

} // Class ends here
