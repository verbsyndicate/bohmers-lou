<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

trait MetaBoxCheckboxMultiple {

    public static function render_checkbox_multiple($args) {
        $id = $args['id'];
        $label = $args['label'];
        $name = $args['name'];
        $options = $args['options'];
        $ref = $args['ref'];
        $value = $args['value'];
        $value_escaped = $args['value_escaped'];

        echo '<div class="checkbox_multiple taxonomydiv" id="' . $id . '">';
        echo '<ul class="category-tabs"><li class="tabs">' . $label . '</li></ul>';
        echo '<div class="tabs-panel">';
        echo '<ul class="categorychecklist form-no-clear">';

        $values = $value ? json_decode($value) : array();

        foreach ($options as $option) {
            $checked = in_array($option['id'], $values) ? 'checked' : '';
            $option_value = $option['id'];
            $option_id = $ref . '-' . $option_value;
            $option_title = $option['title'];

            echo '<li>';
            echo '<input type="checkbox" value="' . $option_value . '" id="' . $option_id . '" name="' . $option_id . '" class="meta-radio" ' . $checked . '/> ';
            echo '<label for="' . $option_id .'">' . $option_title . '</label>';
            echo '</li>';
        }

        echo '</ul></div>';
        ?>
        <script>
            jQuery(function($) {
                $('.checkbox_multiple input[type="checkbox"]').change(function(e) {
                    var arr = [];
                    var $parent = $(this).parents('.checkbox_multiple');
                    $parent.find('input[type="checkbox"]').each(function() {
                        if (!$(this).prop('checked')) {
                            return;
                        }
                        var id = $(this).val();
                        arr.push(id);
                    });
                    var arrAsString = JSON.stringify(arr);
                    $parent.find('input.checkbox_value').val(arrAsString);
                });
            });
        </script>
        <?php

        echo '<input type="hidden" value="' . $value_escaped . '" name="' . $ref . '" id="' . $id .'" class="checkbox_value" />';
        echo '</div>';
    }
}
