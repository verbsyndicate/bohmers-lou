<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

trait MetaBoxCheckbox {

    public static function render_checkbox($args) {
        $id = $args['id'];
        $label = $args['label'];
        $name = $args['name'];
        $ref = $args['ref'];
        $value = $args['value'];
        $value_escaped = $args['value_escaped'];

        if ($value == 'true') {
            $checked = 'checked="checked"';
        } elseif ($default === 'true') {
            $checked = 'checked="checked"';
        } else {
            $checked = '';
        }

        echo '<input type="checkbox" class="meta-radio" name="' . $ref . '" id="' . $id . '" value="true" ' . $checked . ' /> ';
        echo $label !== '' ? '<label for="' . $id . '" class="label">' . $label . '</label>' : '';
        echo '<br />';
    }
}
