<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

trait MetaBoxDatepicker {

    public static function render_datepicker($args) {
        $id = $args['id'];
        $name = $args['name'];
        $ref = $args['ref'];
        $value = $args['value'];
        $value_escaped = $args['value_escaped'];

        $date = new DateTime($value_escaped);
        echo '<input class="datepicker" type="text" name="' . $ref . '-datepicker" id="' . $id . '-datepicker" value="' . $date->format('d-m-Y') . '" />';
        echo '<input type="hidden" name="' . $ref . '" id="' . $id . '" value="' . $value . '" />';
        ?>

        <script>
            jQuery(document).ready(function($) {
                $('#<?php echo $id; ?>-datepicker').datepicker({
                    dateFormat: 'dd-mm-yy',
                    altField: '#<?php echo $id; ?>',
                    altFormat: 'yymmdd',
                });
            });
        </script>
    <?php
    }
}
