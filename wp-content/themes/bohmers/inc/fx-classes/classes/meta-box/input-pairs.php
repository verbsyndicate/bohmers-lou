<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

trait MetaBoxInputPairs {

    public static function render_input_pairs($args) {
        $id = $args['id'];
        $name = $args['name'];
        $ref = $args['ref'];
        $value = $args['value'];
        $value_escaped = $args['value_escaped'];

        echo '<div class="input-pairs" id="input-pairs_' . $id . '">';
        echo '<input type="hidden" value="' . htmlspecialchars($value) . '" name="' . $ref .'" id="input_' . $ref . '" class="regular-text input-pairs_input" />';
        echo '<button type="button" class="button button-primary input-pairs_trigger">Add</button>'

    ?>
        <script type="text/javascript">
            jQuery(function($) {
                var metaBox = $('#input-pairs_<?php echo $id; ?>'),
                    metaTrigger = metaBox.find('.input-pairs_trigger'),
                    metaPreview = metaBox.find('.input-pairs__preview'),
                    metaInput = metaBox.find('.input-pairs_input');

                var valuesArr = metaInput.val() ? JSON.parse(metaInput.val()) : [];

                metaTrigger.click(function() {
                    var newValuesArr = valuesArr.concat([{
                        id: Date.now(),
                        title: '',
                        value: '',
                    }]);

                    update(newValuesArr);
                });

                function update(newValuesArr) {
                    // Update attachments array
                    valuesArr = newValuesArr;

                    // Update meta input
                    metaInput.val(JSON.stringify(newValuesArr));

                    // Re-render UI
                    updateView();
                }

                function updateItem(id) {
                    // Get input fields for id
                    var title = $('#item-title-' + id).val()
                    var value = $('#item-value-' + id).val();

                    // Find and update item in attachment array
                    var updatedValuesArr = valuesArr.map(function(el) {
                        var item = el;
                        if (item.id === parseInt(id)) {
                            item.title = title;
                            item.value = value;
                        }

                        return item;
                    });

                    // Update meta input
                    metaInput.val(JSON.stringify(updatedValuesArr));
                }

                function removeItem(id) {
                    // Create new attachments array by finding and removing specified id
                    var updatedValuesArr = valuesArr.filter(function(el) {
                        return el.id !== parseInt(id);
                    });

                    update(updatedValuesArr);
                }

                function renderItem(item) {
                    var removeButton = '<button type="button" class="button input-pairs__remove">Remove</button>';

                    var titleInput = '<div class="input-pairs__input"><label for="item-title-' + item.id + '">Title</label><input id="item-title-' + item.id + '" type="text" name="title-' + item.id + '" value="' + item.title + '" /></div>';
                    var valueInput = '<div class="input-pairs__input"><label for="item-value-' + item.id + '">Description</label><textarea id="item-value-' + item.id + '" type="text" name="item-value-' + item.id + '">' + item.value + '</textarea></div>';

                    return '<div class="input-pairs__item row" data-id="' + item.id + '"><div class="col-sm-6">' + titleInput + '</div><div class="col-sm-6">'+ valueInput + '</div><div class="col-sm-12">' + removeButton + '</div></div>';
                }

                function updateView() {
                    // Add values
                    if (!valuesArr.length) {
                        return;
                    }

                    // Empty first
                    metaPreview.html('');

                    for (var i = 0; i < valuesArr.length; i++) {
                        var html = renderItem(valuesArr[i]);
                        metaPreview.append(html);
                    }

                    // Add remove button listeners for items
                    $('.input-pairs__remove').click(function() {
                        var $parent = $(this).parents('.input-pairs__item');
                        var id = $parent.attr('data-id');
                        $parent.remove();

                        removeItem(id);
                    });

                    // Update meta input on any input change
                    $('.input-pairs__input').find('input, textarea').change(function() {
                        var $parent = $(this).parents('.input-pairs__item');
                        var id = $parent.attr('data-id');

                        updateItem(id);
                    });
                }

                // On ready render preview
                updateView();

            });
        </script>
        <?php
            echo '<br /><br /><div class="input-pairs__preview"></div>';
            echo '</div>';
    }
}
