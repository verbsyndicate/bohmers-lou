<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

trait MetaBoxLocation {

    public static function render_location($args) {
        $google_map_api_key = GOOGLE_API_KEY;

        $id = $args['id'];
        $name = $args['name'];
        $ref = $args['ref'];
        $value = $args['value'];
        $value_escaped = $args['value_escaped'];

        $values = json_decode($value);
        if (is_object($values)) {
            $address = $values->address;
            $lat = $values->lat;
            $lng = $values->lng;
        }

        echo '<div class="location__input" id="location__input_' . $id . '">';
        echo '
            <p>
                <input class="regular-text location__address" type="text" name="' . $ref . '-address" id="' . $id . '-address" value="' . $address . '" />
                <br />
                <sub>
                    Latitude: <span class="location__lat">' . $lat . '</span>
                    Longitude: <span  class="location__lng">' . $lng . '</span>
                </sub>
            </p>';
        echo '<button type="button" class="button location__trigger">Calculate Location</button>';
        echo '<p class="location__response"></p>';
    ?>
        <script>
        jQuery(function($) {
            var submitting = false,
                key = '<?php echo $google_map_api_key; ?>',
                locationBox = $('#location__input_<?php echo $id; ?>'),
                locationInput = locationBox.find('.location__address'),
                locationTrigger = locationBox.find('.location__trigger'),
                locationResponse = locationBox.find('.location__response'),
                locationLat = locationBox.find('.location__lat'),
                locationLng = locationBox.find('.location__lng'),
                locationResult = locationBox.find('.location__result');

            var currentValue = locationResult.val();
            function locationData(value) {
                if (!value) return;
                value = JSON.parse(value);
                locationInput.val(value.address);
                locationLat.text(parseFloat(value.lat).toFixed(5));
                locationLng.text(parseFloat(value.lng).toFixed(5));
            }

            // Add current data;
            locationData(currentValue);

            $(locationTrigger).click(function() {
                if (submitting) return;
                var address = locationInput.val().replace(' ', '+')
                var url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&key=' + key;

                locationResponse.html('Getting location details.');

                $.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                }).done(function(res) {
                    var data = {
                        address: res.results[0].formatted_address,
                        lat: res.results[0].geometry.location.lat,
                        lng: res.results[0].geometry.location.lng
                    };
                    var result = JSON.stringify(data);
                    // Update input
                    locationResult.val(result);
                    // Re-render UI
                    locationData(result);
                    locationResponse.html('Address successfully added.');
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    locationResponse.html(jqXHR.responseJSON.error_message);
                }).always(function() {
                    submitting = false;
                });
            });
        });
        </script>
    <?php
        echo '<input type="hidden" class="location__result" name="' . $ref . '" id="' . $id . '" value="' . $value_escaped . '" />';

        echo '</div>';
    }
}
