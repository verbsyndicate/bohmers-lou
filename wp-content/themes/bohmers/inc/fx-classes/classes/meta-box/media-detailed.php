<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

trait MetaBoxMediaDetailed {

    public static function render_media_detailed($args) {
        $button_text = $args['button_text'] ? $args['button_text'] : 'Add media';
        $id = $args['id'];
        $name = $args['name'];
        $ref = $args['ref'];
        $value = $args['value'];
        $value_escaped = $args['value_escaped'];

        echo '<div class="select-media" id="select-media_' . $id . '">';
        echo '<input type="hidden" value="' . htmlspecialchars($value) . '" name="' . $ref .'" id="input_' . $ref . '" class="regular-text select-media_input" />';
        echo '<button type="button" class="button button-primary select-media_trigger">' . $button_text . '</button>';
    ?>
        <script type="text/javascript">
            jQuery(function($) {
                var frame,
                    metaBox = $('#select-media_<?php echo $id; ?>'),
                    metaTrigger = metaBox.find('.select-media_trigger'),
                    metaPreview = metaBox.find('.select-media__preview'),
                    metaInput = metaBox.find('.select-media_input');

                var attachmentsArr = metaInput.val() ? JSON.parse(metaInput.val()) : [];

                metaTrigger.click(function() {
                    if (frame) {
                        frame.open();
                        return;
                    }

                    frame = wp.media({
                        title: 'Select or Upload Media',
                        button: {
                            text: 'Use this media'
                    },
                        multiple: true
                    });

                    frame.on( 'select', function() {
                        var attachments = frame.state().get('selection').toJSON();
                        for (var i = 0; i < attachments.length; i++) {
                            var newAttachment = {
                                id: attachments[i].id,
                                url: attachments[i].url,
                                title: attachments[i].title,
                                description: attachments[i].description,
                                type: attachments[i].type
                            };

                            var mediaExists = attachmentsArr.filter(function(attachment) {
                                return newAttachment.id === attachment.id
                            });

                            if (mediaExists.length > 0) continue;
                            attachmentsArr.push(newAttachment);
                        }
                        updateMedia(attachmentsArr);
                    });

                    frame.open();
                });

                function updateMedia(newAttachmentsArr) {
                    // Update attachments array
                    attachmentsArr = newAttachmentsArr;

                    // Update meta input
                    metaInput.val(JSON.stringify(newAttachmentsArr));

                    // Re-render UI
                    updatePreview();
                }

                function updateMediaItem(id) {
                    // Get input fields for id
                    var title = $('#media-title-' + id).val()
                    var description = $('#media-description-' + id).val();

                    // Find and update item in attachment array
                    var updatedAttachmentsArr = attachmentsArr.map(function(el) {
                        var item = el;
                        if (item.id === parseInt(id)) {
                            item.title = title;
                            item.description = description;
                        }

                        return item;
                    });

                    // Update meta input
                    metaInput.val(JSON.stringify(updatedAttachmentsArr));
                }

                function removeMedia(id) {
                    // Create new attachments array by finding and removing specified id
                    var updatedAttachmentsArr = attachmentsArr.filter(function(el) {
                        return el.id !== parseInt(id);
                    });

                    // Re-render UI
                    updateMedia(updatedAttachmentsArr);
                }

                function renderMedia(item) {
                    var type = item.type;
                    var removeButton = '<button type="button" class="button media-detailed__remove">Remove</button>';

                    var imageHtml = '<div class="media__preview" style="background-image:url(' + item.url + ')"></div>';

                    if (type !== 'image') {
                        imageHtml = '<div>File: ' + item.title + '</div>';
                    }

                    var titleInput = '<div class="media-detailed__input"><label for="media-title-' + item.id + '">Title</label><input id="media-title-' + item.id + '" type="text" name="title-' + item.id + '" value="' + item.title + '" /></div>';
                    var descriptionInput = '<div class="media-detailed__input"><label for="media-description-' + item.id + '">Description</label><textarea id="media-description-' + item.id + '" type="text" name="media-description-' + item.id + '">' + item.description + '</textarea></div>';

                    return '<div class="media__attachment media__attachment--detailed row" data-id="' + item.id + '"><div class="col-sm-4">' + imageHtml + '</div><div class="col-sm-8">' + titleInput + descriptionInput + removeButton + '</div></div>';
                }

                function updatePreview() {
                    // Empty first
                    metaPreview.html('');

                    // Add all attachments
                    for (var i = 0; i < attachmentsArr.length; i++) {
                        var html = renderMedia(attachmentsArr[i]);
                        metaPreview.append(html);
                    }

                    // Add remove button listeners for items
                    $('.media-detailed__remove').click(function() {
                        var $parent = $(this).parents('.media__attachment');
                        var id = $parent.attr('data-id');
                        $parent.remove();

                        removeMedia(id);
                    });

                    // Update meta input on any input change
                    $('.media-detailed__input').find('input, textarea').change(function() {
                        var $parent = $(this).parents('.media__attachment');
                        var id = $parent.attr('data-id');

                        updateMediaItem(id);
                    });
                }

                // On ready render preview
                updatePreview();

            });
        </script>
        <?php
            echo '<br /><br /><div class="select-media__preview"></div>';
            echo '</div>';
    }
}
