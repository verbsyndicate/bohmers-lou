<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

trait MetaBoxMediaMultiple {

    public static function render_media_multiple($args) {
        $button_text = $args['button_text'] ? $args['button_text'] : 'Add media';
        $id = $args['id'];
        $name = $args['name'];
        $ref = $args['ref'];
        $value = $args['value'];
        $value_escaped = $args['value_escaped'];

        echo '<div class="select-media" id="select-media_' . $id . '">';
        echo '<input type="hidden" value="' . htmlspecialchars($value) . '" name="' . $ref .'" id="input_' . $ref . '" class="regular-text select-media_input" />';
        echo '<button type="button" class="button button-primary select-media_trigger">' . $button_text .'</button>';
    ?>
        <script type="text/javascript">
            jQuery(function($) {
                var frame,
                    metaBox = $('#select-media_<?php echo $id; ?>'),
                    metaTrigger = metaBox.find('.select-media_trigger'),
                    metaPreview = metaBox.find('.select-media__preview'),
                    metaInput = metaBox.find('.select-media_input');

                var attachmentsArr = metaInput.val() ? JSON.parse(metaInput.val()) : [];

                metaTrigger.click(function() {
                    if (frame) {
                        frame.open();
                        return;
                    }

                    frame = wp.media({
                        title: 'Select or Upload Media',
                        button: {
                            text: 'Use this media'
                    },
                        multiple: true
                    });

                    frame.on( 'select', function() {
                        var attachments = frame.state().get('selection').toJSON();
                        for (var i = 0; i < attachments.length; i++) {
                            var newAttachment = {
                                id: attachments[i].id,
                                url: attachments[i].url,
                                title: attachments[i].title,
                                description: attachments[i].description,
                                type: attachments[i].type
                            };

                            var mediaExists = attachmentsArr.filter(function(attachment) {
                                return newAttachment.id === attachment.id
                            });

                            if (mediaExists.length > 0) continue;
                            attachmentsArr.push(newAttachment);
                        }
                        updateMedia(attachmentsArr);
                    });

                    frame.open();
                });

                function updateMedia(newAttachmentsArr) {
                    // Update attachments array
                    attachmentsArr = newAttachmentsArr;

                    // Update meta input
                    metaInput.val(JSON.stringify(newAttachmentsArr));

                    // Re-render UI
                    updatePreview();
                }

                function removeMedia(id) {
                    var updatedAttachmentsArr = attachmentsArr.filter(function (el) {
                        return el.id !== parseInt(id);
                    });

                    // Re-render UI
                    updateMedia(updatedAttachmentsArr);
                }

                function updatePreview() {
                    // Empty first
                    metaPreview.html('');

                    // Add all attachments
                    for (var i = 0; i < attachmentsArr.length; i++) {

                        var type = attachmentsArr[i].type,
                            html = '',
                            closeHtml = '<button type="button" class="select-media__remove">&times;</button>';

                        if (type === 'image') {
                            html = '<div class="media__attachment" data-id="' + attachmentsArr[i].id + '"><div class="media__preview" style="background-image:url(' + attachmentsArr[i].url + ')"></div>' + closeHtml + '</div>';
                        } else {
                            html = '<div class="media__attachment media__attachment--row" data-id="' + attachmentsArr[i].id + '">' +
                                'File: ' + attachmentsArr[i].title +
                                closeHtml +
                                '</div>';
                        }

                        metaPreview.append(html);
                    }

                    // Delete button handler
                    $('.select-media__remove').click(function() {
                        var $parent = $(this).parents('.media__attachment');
                        var id = $parent.attr('data-id');
                        $parent.remove();

                        removeMedia(id);
                    });
                }

                // On ready set preview
                updatePreview();

            });
        </script>
        <?php
            echo '<br /><br /><div class="select-media__preview"></div>';
            echo '</div>';
    }
}
