<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

trait MetaBoxMedia {

    public static function render_media($args) {
        $button_text = $args['button_text'] ? $args['button_text'] : 'Select media';
        $id = $args['id'];
        $name = $args['name'];
        $ref = $args['ref'];
        $value = $args['value'];
        $value_escaped = $args['value_escaped'];
        $value_as_object = $args['value_as_object'];

        // Useful value if object
        $value_object = $value_as_object ? json_decode($value) : null;

        $media_url = $value_as_object ? $value_object->url : $value;

        echo '<div class="select-media" id="select-media_' . $id . '">';
        echo '<input type="hidden" value="' . htmlspecialchars($value) . '" name="' . $ref .'" id="input_' . $id . '" class="select-media_input" />';
        echo ' <button type="button" class="button select-media_trigger">' . $button_text . '</button> ';
        echo '<button type="button" class="button select-media_delete" ' . (!$value ? 'style="display:none;"' : '') . '> Remove</button>';
    ?>
        <script type="text/javascript">
            jQuery(function($) {
                var frame,
                    metaBox = $('#select-media_<?php echo $id; ?>'), // Your meta box id here
                    metaTrigger = metaBox.find('.select-media_trigger'),
                    metaDelete = metaBox.find('.select-media_delete'),
                    metaPreview = metaBox.find('.select-media__preview'),
                    metaInput = metaBox.find('.select-media_input');

                metaTrigger.on('click', function(e) {
                    e.preventDefault();

                    if (frame) {
                        frame.open();
                        return;
                    }

                    frame = wp.media({
                        title: 'Select or Upload Media',
                        button: {
                            text: 'Use this media'
                    },
                        multiple: false
                    });

                    frame.on( 'select', function() {
                        var attachment = frame.state().get('selection').first().toJSON();
                        if (attachment.url.match(/(\.jpg|\.jpeg|\.png|\.bmp)$/) != null) {
                            metaPreview.html('<img src="'+attachment.url+'" alt="" style="max-width:100%;"/>');
                        } else {
                            metaPreview.html('<strong>File: </strong>' + attachment.url);
                        }

                        <?php if ($value_as_object) : ?>
                        // Save whole attachment object
                        metaInput.val(JSON.stringify(attachment));
                        <?php else : ?>
                        // Just save attachment url
                        metaInput.val(attachment.url);
                        <?php endif; ?>

                        metaDelete.show();
                    });

                    frame.open();
                });

                metaDelete.on('click', function(e) {
                    e.preventDefault();
                    metaPreview.html('');
                    metaDelete.hide();
                    metaInput.val('');
                });
            });
        </script>
        <?php
        echo '<br /><br /><div class="select-media__preview" style="width: 200px;">';

        if (preg_match('/(\.jpg|\.jpeg|\.png|\.bmp|\.svg)$/', $media_url)) {
            echo '<img src="' . $media_url . '" alt="" style="max-width:100%;"/>';
        } elseif ($media_url) {
            echo '<strong>File: </strong>' . $media_url;
        }

        echo '</div>';
        echo '</div>';
    }
}
