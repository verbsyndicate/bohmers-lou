<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

trait MetaBoxSelectIcon {

    public static function render_select_icon($args) {
        $id = $args['id'];
        $name = $args['name'];
        $options = $args['options'];
        $ref = $args['ref'];
        $value = $args['value'];
        $value_escaped = $args['value_escaped'];

        $button_class = $value ? 'icon--' . $value : 'no-icon';

        echo '<div class="select-icon">';
        echo '<input type="hidden" name="' . $ref . '" id="' . $id . '" value="' . $value . '" />';
        echo '<button type="button" class="icon-picker ' . $button_class . '" data-target="#' . $id . '"></button>';
        echo '</div>';
    }
}
