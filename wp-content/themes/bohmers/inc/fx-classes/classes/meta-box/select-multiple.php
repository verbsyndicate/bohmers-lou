<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

trait MetaBoxSelectMultiple {

    public static function render_select_multiple($args) {
        $id = $args['id'];
        $n_options = $args['n_options'];
        $options = $args['options'];
        $name = $args['name'];
        $ref = $args['ref'];
        $value = $args['value'];
        $value_escaped = $args['value_escaped'];

        $values = json_decode($value);
        // set initial values
        while (count($values) < $n_options) {
            $values[] = 0;
        }

        echo '<div class="select_multiple">';

        $i = 0;
        foreach ($values as $v) {
            if ($i === $n_options) {
                return;
            }

            $i++;

            echo "<select name='select_multiple_select'>";

            foreach ($options as $option) :
                if (is_array($option)) {
                    $id = isset($option['id']) ? $option['id'] : '';
                    $title = isset($option['title']) ? $option['title'] : '';
                    $post_type = isset($option['post_type']) ? '(' . $option['post_type'] . ')' : '';
                } else {
                    $id = $option;
                    $title = $option;
                }

                $selected = $v == $id ? 'selected' : '';

                echo '<option value="' . $id . '" ' . $selected . '>' . $title . $post_type . '</option>';

            endforeach;

            echo '</select><br />';
        }

        ?>
        <script>
            jQuery(function($) {
                $('.select_multiple select').change(function(e) {
                    var arr = [];
                    $parent = $(this).parents('.select_multiple');
                    $parent.find('select').each(function() {
                        var id = $(this).val();
                        arr.push(id);
                    });
                    var arrAsString = JSON.stringify(arr);
                    $parent.find('input.select_value').val(arrAsString);
                });
            });
        </script>
        <?php

        echo '<input type="hidden" value="' . $value_escaped . '" name="' . $ref . '" id="' . $id .'" class="select_value" />';

        echo '</div>';
    }
}
