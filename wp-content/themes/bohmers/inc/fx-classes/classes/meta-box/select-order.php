<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

trait MetaBoxSelectOrder {

    public static function render_select_order($args) {
        $id = $args['id'];
        $name = $args['name'];
        $options = $args['options'];
        $ref = $args['ref'];
        $value = $args['value'];
        $value_escaped = $args['value_escaped'];

        echo '<div class="select-order" id="select-order_' . $id . '">';
        echo '<select name="select-order_' . $ref . '" class="select-order__select">';
        foreach ($options as $opt) :
            if (is_array($opt)) {
                $option_id = $opt['id'];
                $option_slug = $opt['slug'];
                $option_title = $opt['title'];
            } else {
                $option_id = $opt;
                $option_slug = $opt;
                $option_title = $opt;
            }
            echo '<option value="' . $option_id . '" data-slug-value="' . $option_slug . '" name="' . $option_id . '_value" ' . ($value == $option_id ? 'selected' : '') . '>' . $option_title  . '</option>';
        endforeach;

        echo '</select>&nbsp;';
        echo '<button type="button" class="button select-order_trigger">Add</button>';
        ?>
        <script>
        jQuery(function($) {
            var metaOrderBox = $('#select-order_<?php echo $id; ?>'), // Your meta box id here
                metaOrderSelect = metaOrderBox.find('.select-order__select'),
                metaOrderTrigger = metaOrderBox.find('.select-order_trigger'),
                metaOrderPreview = metaOrderBox.find('.select-order__preview'),
                metaOrderInput = metaOrderBox.find('.select-order_input');

            renderSelectOrderList();

            metaOrderTrigger.click(function(e) {
                var currentValues = metaOrderInput.val() ? JSON.parse(metaOrderInput.val()) : [];
                var newValue = {
                    id: metaOrderSelect.val(),
                    slug: metaOrderSelect.find('option:selected').attr('data-slug-value'),
                    title: metaOrderSelect.find('option:selected').html()
                };
                currentValues.push(newValue)
                var arrAsString = JSON.stringify(currentValues);
                metaOrderInput.val(arrAsString);
                renderSelectOrderList();
            });

            function AddItemClickListener() {
                metaOrderBox.find('.select-order__item').click(function(e) {
                    var currentValues = metaOrderInput.val() ? JSON.parse(metaOrderInput.val()) : [];
                    var removeIndex = $(this).attr('data-id');
                    if (removeIndex > -1) {
                        currentValues.splice(removeIndex, 1);
                    }
                    var arrAsString = JSON.stringify(currentValues);
                    metaOrderInput.val(arrAsString);
                    renderSelectOrderList();
                });
            }

            function renderSelectOrderList() {
                metaOrderPreview.html('');
                if (!metaOrderInput.val()) {
                    return;
                }

                var list = JSON.parse(metaOrderInput.val());

                for (var i = 0; i < list.length; i++) {
                    var html = '<button type="button" data-id="' + i + '" class="select-order__item">'
                        + list[i].title
                        + '<span>&times;</span>'
                        + '</button>';
                    metaOrderPreview.append(html);
                }

                // Add button events
                AddItemClickListener();
            }
        });
        </script>
        <?php
        echo '<div class="select-order__preview"></div>';
        echo '<input type="hidden" value="' . $value_escaped . '" name="' . $ref . '" id="' . $id .'" class="select-order_input" />';
        echo '</div>';
    }
}
