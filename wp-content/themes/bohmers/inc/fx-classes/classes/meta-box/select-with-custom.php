<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

trait MetaBoxSelectWithCustom {

    public static function render_select_with_custom($args) {
        $id = $args['id'];
        $name = $args['name'];
        $options = $args['options'];
        $ref = $args['ref'];
        $value = $args['value'];
        $value_escaped = $args['value_escaped'];

        echo '<div class="select-with-custom">';
        echo '<select name="select-with-custom_' . $ref . '" id="select-with-custom_' . $id . '">';
        echo '<option value="custom" name="custom">Custom URL</option>';

        foreach ($options as $opt) {
            echo '<option value="' . $opt['id'] . '" name="' . $opt['id'] . '" ' . ($value_escaped == $opt['id'] ? 'selected' : '') . '>' . $opt['title'] . ' (' . $opt['post_type'] . ')</option>';
        }

        echo '</select>';
        echo '<p>Select existing content, or enter a url.</p>';
        echo '<div class="select-with-custom__input-wrap">';
        echo '<input type="text" name="' . $ref . '" id="' . $id . '" value="' . $value_escaped . '" />';
        echo '</div>';
        ?>
        <script>
        jQuery(function($) {
            $("#select-with-custom_<?php echo $id; ?>").change(function() {
                var selectValue = $(this).val();
                var inputValue = selectValue !== 'custom' ? selectValue : null;
                if (inputValue) {
                    $("#<?php echo $id; ?>").val(inputValue);
                }

                var $inputWrapEl = $(this).parents('.select-with-custom').find('.select-with-custom__input-wrap');
                if (!inputValue) {
                    $inputWrapEl.slideDown(200);
                } else {
                    $inputWrapEl.slideUp(200);
                }
            });

            $("#select-with-custom_<?php echo $id; ?>").trigger('change');

        });
        </script>
        <?php
        echo '</div>';
    }
}
