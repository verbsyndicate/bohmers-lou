<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

trait MetaBoxSelect {

    public static function render_select($args) {
        $exclude_empty_option = $args['exclude_empty_option'];
        $id = $args['id'];
        $name = $args['name'];
        $options = $args['options'];
        $ref = $args['ref'];
        $value = $args['value'];
        $value_escaped = $args['value_escaped'];

        echo '<select name="' . $ref . '" id="' . $id . '">';

        if (!$exclude_empty_option) {
            echo '<option value="" name="">Please select...</option>';
        }

        foreach ($options as $option) :
            if (is_array($option)) {
                $id = isset($option['id']) ? $option['id'] : '';
                $title = isset($option['title']) ? $option['title'] : '';
            } else {
                $id = $option;
                $title = $option;
            }

            $selected = $value == $id ? 'selected' : '';

            echo '<option value="' . $id . '" name="' . $id . '_value" ' . $selected . '>' . $title  . '</option>';

        endforeach;

        echo '</select>';
        echo '<br />';
    }
}
