<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

trait MetaBoxTextarea {

    public static function render_textarea($args) {
        $id = $args['id'];
        $readonly = $args['readonly'] ? 'readonly="true"' : '';
        $ref = $args['ref'];
        $value = $args['value'];
        $value_escaped = $args['value_escaped'];

        echo '<textarea rows="4" style="width:98%" name="' . $ref . '" id="' . $id . '" ' . $readonly . '>' . $value . '</textarea>';
        echo '<br />';
    }
}
