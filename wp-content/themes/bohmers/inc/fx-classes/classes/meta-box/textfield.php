<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

trait MetaBoxTextfield {

    public static function render_textfield($args) {
        $id = $args['id'];
        $input_type = isset($args['input_type']) ? $args['input_type'] : 'text';
        $readonly = $args['readonly'] ? 'readonly="true"' : '';
        $ref = $args['ref'];
        $value = $args['value'];
        $value_escaped = $args['value_escaped'];

        echo '<input type="' . $input_type . '" name="' . $ref . '" id="' . $id . '" value="' . $value_escaped . '" ' . $readonly . ' />';
        echo '<br />';
    }
}
