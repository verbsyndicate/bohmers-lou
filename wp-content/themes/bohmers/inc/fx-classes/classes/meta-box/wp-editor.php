<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

trait MetaBoxWpEditor {

    public static function render_wp_editor($args) {
        $ref = $args['ref'];
        $value = $args['value'];

        echo wp_editor($value, $ref);
    }
}
