<?php

/*
Types available:
    checkbox
    checkbox-multiple
    datepicker
    input-pairs
    location
    media
    media-detailed
    media-multiple
    select
    select-icon
    select-multiple
    select-order
    select-with-custom
    textarea (default input)
    textfield
    wp-editor
*/

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

trait MetaBoxes {
    // Get helpers
    use MetaHelpers;

    // Load all components
    use MetaBoxCheckbox,
        MetaBoxCheckboxMultiple,
        MetaBoxDatepicker,
        MetaBoxInputPairs,
        MetaBoxLocation,
        MetaBoxLocationMap,
        MetaBoxMedia,
        MetaBoxMediaDetailed,
        MetaBoxMediaMultiple,
        MetaBoxSelect,
        MetaBoxSelectIcon,
        MetaBoxSelectMultiple,
        MetaBoxSelectOrder,
        MetaBoxSelectWithCustom,
        MetaBoxTextarea,
        MetaBoxTextfield,
        MetaBoxWpEditor;

    public static function render_input($args) {
        $class = $args['class'];
        $description = $args['description'];
        $name = $args['name'];
        $title = $args['title'];
        $type = $args['type'];

        echo '<div class="meta-field ' . $class . '">';

        echo '<input type="hidden" name="' . $name . '_noncename" id="' . $name . '_noncename" value="' . wp_create_nonce() . '" />';

        echo $title ? '<div class="meta-field__title">' . $title . '</div>' : '';

        // Checkbox
        if ($type === 'checkbox') {

            MetaBoxes::render_checkbox($args);

            // Checkbox Multiple
        } elseif ($type === 'checkbox-multiple') {

            MetaBoxes::render_checkbox_multiple($args);

            // Datepicker
        } elseif ($type === 'datepicker') {

            MetaBoxes::render_datepicker($args);

            // input-pairs Multiple
        } elseif ($type === 'input-pairs') {

            MetaBoxes::render_input_pairs($args);

            // Location
        } elseif ($type === 'location') {

            MetaBoxes::render_location($args);

            // Location Map
        } elseif ($type === 'location-map') {

            MetaBoxes::render_location_map($args);

            // Media
        } elseif ($type === 'media') {

            MetaBoxes::render_media($args);

            // Media Detailed
        } elseif ($type === 'media-detailed') {

            MetaBoxes::render_media_detailed($args);

            // Media Multiple
        } elseif ($type === 'media-multiple') {

            MetaBoxes::render_media_multiple($args);

            // Select
        } elseif ($type == 'select') {

            MetaBoxes::render_select($args);

            // Select Icon
        } elseif ($type === 'select-icon') {

            MetaBoxes::render_select_icon($args);

            // Select Multiple
        } elseif ($type === 'select-multiple') {

            MetaBoxes::render_select_multiple($args);

            // Select & Order
        } elseif ($type ==='select-order') {

            MetaBoxes::render_select_order($args);

            // Select With Custom
        } elseif ($type === 'select-with-custom') {

            MetaBoxes::render_select_with_custom($args);

            // Textarea
        } elseif ($type === 'textarea') {

            MetaBoxes::render_textarea($args);

            // WP Editor
        } elseif ($type === 'wp-editor') {

            MetaBoxes::render_wp_editor($args);

            // Textfield (default)
        } else {

            MetaBoxes::render_textfield($args);

        }

        echo $description !== '' ? '<p class="description">' . $description . '</p>' : '';

        echo '</div>';

        echo $spacing;
    }
}
