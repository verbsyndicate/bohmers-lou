<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

trait MetaHelpers {

    public function get_meta_options($options) {
        if (is_array($options)) {
            return $options;
        }

        if (is_string($options)) {
            if ($options === 'all') {
                return $this::get_posts_helper();
            }

            $options_string = preg_replace('/\s+/', '', $options);
            $options_array = explode(',', $options_string);

            return $this::get_posts_helper($options_array);
        }

        if (is_callable($options)) {
            return $options();
        }
    }

    static function get_posts_helper($post_types = array()) {
        $options = array();
        $all_post_types = get_post_types();
        // remove unwanted post types
        unset($all_post_types['attachment']);
        unset($all_post_types['revision']);
        unset($all_post_types['nav_menu_item']);

        $query_post_types = $post_types ? $post_types : $all_post_types;
        $posts = get_posts(array(
            'post_type' => $query_post_types,
            'posts_per_page' => -1
        ));

        foreach ($posts as $post) : setup_postdata($post);
            $options[] = array(
                'id' => $post->ID,
                'slug' => $post->post_name,
                'title' => $post->post_title,
                'post_type' => $post->post_type,
            );
        endforeach;

        wp_reset_postdata();

        return $options;
    }
}
