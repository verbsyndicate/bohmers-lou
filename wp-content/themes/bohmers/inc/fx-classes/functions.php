<?php
/*
 *  Author: Mitch Heddles
 *  URL: http://mitchheddles.com
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

/*------------------------------------*\
	Constants
\*------------------------------------*/

require_once('classes/constants.php');

/*------------------------------------*\
	External Traits/Classes
\*------------------------------------*/

// Individual meta boxes
require_once('classes/meta-box/checkbox-multiple.php');
require_once('classes/meta-box/checkbox.php');
require_once('classes/meta-box/datepicker.php');
require_once('classes/meta-box/input-pairs.php');
require_once('classes/meta-box/location.php');
require_once('classes/meta-box/location-map.php');
require_once('classes/meta-box/media-detailed.php');
require_once('classes/meta-box/media-multiple.php');
require_once('classes/meta-box/media.php');
require_once('classes/meta-box/select-icon.php');
require_once('classes/meta-box/select-multiple.php');
require_once('classes/meta-box/select-order.php');
require_once('classes/meta-box/select-with-custom.php');
require_once('classes/meta-box/select.php');
require_once('classes/meta-box/textarea.php');
require_once('classes/meta-box/textfield.php');
require_once('classes/meta-box/wp-editor.php');

// Load helpers and meta box traits
require_once('classes/trait-meta-helpers.php');
require_once('classes/trait-meta-boxes.php');

// Now load classes
require_once('classes/class-admin-pages.php');
require_once('classes/class-cpt-meta.php');
require_once('classes/class-cpt.php');
require_once('classes/class-custom-widget.php');
require_once('classes/class-process-form.php');

/*------------------------------------*\
   Enqueue Scripts & Styles
\*------------------------------------*/

function fx_admin_enqueue_scripts_styles() {
    $asset_path = get_template_directory_uri() . '/inc/fx-classes/assets';
    // Admin styles
    wp_enqueue_style('fx-admin-styles', $asset_path . '/css/style.min.css', array(), '1');

    // Admin scripts
    wp_register_script('fx-admin-scripts', $asset_path . '/js/main.min.js', array('jquery'), '1');
    wp_enqueue_script('fx-admin-scripts');

    // Vendor
    wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
}

add_action('admin_enqueue_scripts', 'fx_admin_enqueue_scripts_styles');
