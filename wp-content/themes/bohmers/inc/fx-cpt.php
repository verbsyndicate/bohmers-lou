<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

$services = new CPT('service', array(
    'singular' => 'Service',
    'plural' => 'Services',
    'icon' => 'f107'
));

$services->add_meta_section('icon', array(
        'title' => 'Service Icon',
        'position' => 'side',
        'priority' => 'default',
    ),
    array(
        'icon' => array(
            'type' => 'media',
            'title' => '',
            'description' => '',
            'button_text' => 'Select Icon',
        )
    )
);
$services->add_meta_section('order', array(
    'title' => 'Order Services',
    'position' => 'side',
    'priority' => 'default',
    ),
    array(
      'order' => array(
        'type' => 'number',
        'title' => '',
        'description' => '',
       )
    )
);

$partners = new CPT('partner', array(
    'singular' => 'Partner',
    'plural' => 'Partners',
    'icon' => 'f487',
    'exclude_from_search' => true,
));

$testimonials = new CPT('testimonial', array(
    'singular' => 'Testimonial',
    'plural' => 'Testimonials',
    'icon' => 'f130'
));

$testimonials->add_meta_section('details', array(
        'title' => 'Tesimonial Details',
        'position' => 'normal',
        'priority' => 'high',
    ),
    array(
        'company' => array(
            'type' => 'textfield',
            'title' => 'Tesimonial Company',
            'description' => '',
        )
    )
);

$slides = new CPT('slide', array(
    'singular' => 'Slide',
    'plural' => 'Slides',
    'icon' => 'f232'
));

$slides->add_meta_section('content', array(
        'title' => 'Slide Settings',
        'position' => 'normal',
        'priority' => 'high',
    ),
    array(
        'button_text' => array(
            'type' => 'textfield',
            'title' => 'Button Text',
            'description' => ''
        ),
        'button_link' => array(
            'type' => 'select-with-custom',
            'options' => 'all',
            'title' => 'Button Link',
            'description' => ''
        )
    )
);
