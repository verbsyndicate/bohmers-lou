<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

/*------------------------------------*\
   Scripts
\*------------------------------------*/

function fx_header_scripts() {
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

        wp_deregister_script( 'wp-embed' );

        wp_register_script('fx-scripts', get_template_directory_uri() . '/assets/js/main.js', array('jquery'), ASSET_VERSION);
        wp_enqueue_script('fx-scripts');

        wp_localize_script( 'fx-scripts', 'CONSTANTS', array(
            'API_URL' => get_site_url() . '/wp-json/' . API_BASE . API_VERSION,
            'API_NONCE' => wp_create_nonce(API_NONCE),
            'GOOGLE_API_KEY' => GOOGLE_API_KEY,
        ));
    }
}

/*------------------------------------*\
   Styles
\*------------------------------------*/

function fx_styles() {
    // wp_register_style('fx-style', get_template_directory_uri() . '/assets/css/style.css', array(), ASSET_VERSION, 'all');
    // wp_enqueue_style('fx-style');
    wp_register_style('lou-style', get_template_directory_uri() . '/assets/css/style-unmin.css', array(), ASSET_VERSION, 'all');
    wp_enqueue_style('lou-style');
}

/*------------------------------------*\
   Admin Scripts & Styles
\*------------------------------------*/

function fx_theme_admin_enqueue() {
    // Admin styles
    wp_enqueue_style('fx-theme-admin-styles', get_template_directory_uri().'/assets/css/admin.css', array(), ASSET_VERSION);

    // Admin scripts
    wp_register_script('fx-theme-admin-scripts', get_template_directory_uri() . '/assets/js/admin.js', array('jquery', 'fx-admin-scripts'), ASSET_VERSION);
    wp_enqueue_script('fx-theme-admin-scripts');
}

add_action('admin_enqueue_scripts', 'fx_theme_admin_enqueue');

/*------------------------------------*\
  Move Scripts to Footer
\*------------------------------------*/

function remove_head_scripts() {
    remove_action('wp_head', 'wp_print_scripts');
    remove_action('wp_head', 'wp_print_head_scripts', 9);
    remove_action('wp_head', 'wp_enqueue_scripts', 1);

    add_action('wp_footer', 'wp_print_scripts', 5);
    add_action('wp_footer', 'wp_enqueue_scripts', 5);
    add_action('wp_footer', 'wp_print_head_scripts', 5);
}

add_action('wp_enqueue_scripts', 'remove_head_scripts');

/*------------------------------------*\
   Dequeue jQuery Migrate
\*------------------------------------*/

function fx_dequeue_jquery_migrate( $scripts ) {
    if (! is_admin() && ! empty( $scripts->registered['jquery'])) {
        $jquery_dependencies = $scripts->registered['jquery']->deps;
        $scripts->registered['jquery']->deps = array_diff( $jquery_dependencies, array( 'jquery-migrate' ) );
    }
}

add_action('wp_default_scripts', 'fx_dequeue_jquery_migrate');
