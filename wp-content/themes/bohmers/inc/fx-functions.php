<?php

/**
 * Get Template Directory Shorthand
 * @return string Echos the theme directory
 */
function theme_dir() {
    echo get_template_directory_uri();
}

/**
 * Get Post Meta
 * @param  string   $name Meta name
 * @param  integer  $id   Post id
 * @return mixed          Post meta data response or null
 */
function fx_get_meta($name = '', $id = null) {
    if (empty($name)) {
        return null;
    }

    global $post;
    $id = !empty($id) ? $id : $post->ID;
    $meta_name = '_fx_' . $name . '_value';

    $result = get_post_meta($id, $meta_name, true);

    return $result;
}

/**
 * Get Url from Select Meta
 * @param  mixed $value Value from select, either post id (int) or url string
 * @return mixed        Returns the url or null
 */
function fx_get_url_from_select($value) {
    if (!$value) {
        return null;
    }

    if (intval($value) > 0) {
        return get_permalink($value);
    }

    // Check for #value
    if (substr($value, 0, 1) === '#') {
        return $value;
    }

    return check_url($value);
}

/**
 * Get Theme Option
 * @param  string  $name Theme option name
 * @param  boolean $raw  Get raw data or preformatted
 * @return mixed         Returns theme option data or null
 */
function get_opts($name, $raw = false) {
    $options = fx_get_theme_options();
    $result = isset($options[$name]) ? $options[$name] : null;

    if (!$result) {
        return null;
    }

    if ($raw) {
        return $result;
    }

    switch ($name) {
        case 'street_address':
            return wpautop($result);
            break;
        case 'email':
            return '<a href="mailto:' . antispambot($result) . '">' . antispambot($result) . '</a>';
            break;
        case 'social_facebook':
        case 'social_linkedin':
        case 'social_youtube':
        case 'social_google':
            return check_url($result);
            break;
        case 'social_instagram':
            return '//instagram.com/' . $result;
            break;
        case 'social_twitter':
            return '//twitter.com/' . $result;
            break;
        default:
            return esc_attr($result);
    }
}

/**
 * Get theme options
 * @return array Returns all theme options
 */
function fx_get_theme_options() {
    $result = get_option('theme_options');

    return $result;
}

/**
 * Check url
 * @param  string $url Url
 * @return mixed       Returns original url with leading slashes or null
 */
function check_url($url) {
    if (empty($url)) {
        return null;
    }

    $parsed = parse_url($url);

    if (empty($parsed['scheme'])) {
        $url = '//' . ltrim($url, '/');
    }

    return $url;
}

/**
 * Get post image url
 * @param  string  $size Image size
 * @param  integer $id   Post id, defaults to current $post
 * @return mixed         Returns the image url for a given post and size or null
 */
function fx_post_image_url($size = 'full', $id = 0) {
    global $post;
    $id = $id ? $id : $post->ID;

    if (!has_post_thumbnail($id)) {
        return null;
    }

    $image = wp_get_attachment_image_src( get_post_thumbnail_id($id), $size);

    return $image[0];
}

/**
 * Get post image as background style
 * @param  boolean $echo Echo result
 * @param  integer $id   Post id, defaults to current $post
 * @param  string  $size Image size
 * @return mixed         Echos, or returns, the post image as a background style
 */
function fx_bg_style($echo = true, $id = 0, $size = 'full') {
    $image_url = fx_post_image_url($size, $id);

    if (!$image_url) {
        return '';
    }

    $background = 'background-image:url(' . $image_url . ')';

    if ($echo) {
        echo $background;
        return '';
    }

    return $background;
}

/**
 * Get post blurb
 * @param  integer $n  Number of paragraphs
 * @param  integer $id Post id, defaults to current $post
 * @return string      Returns the first n paragraphs for a post
 */
function fx_post_blurb($n = 1, $id = 0) {
    global $post;
    $id = $id ? $id : $post->ID;

    $content = fx_content_filters(get_post_field('post_content', $id), '<p>');

    if (!$content) {
        return '';
    }

    preg_match_all('/<p>(.*)<\/p>/', $content, $matches);

    $blurb = '';
    for ($i=0; $i<$n; $i++) {
        $blurb .= $matches[0][$i];
    }

    return $blurb;
}

/**
 * Get post introduction
 * @param  integer $n  Number of sentences
 * @param  integer $id Post id, defaults to current $post
 * @return string      Returns the first n sentences for a post
 */
function fx_post_intro($n = 2, $id = 0) {
    global $post;
    $id = $id ? $id : $post->ID;

    $content = fx_content_filters(get_post_field('post_content', $id));
    $content = strip_tags($content);

    if (!$content) {
        return '';
    }

    $re = '/(?<=[.!?]|[.!?][\'"])\s+/';
    $sentences = preg_split($re, $content, -1, PREG_SPLIT_NO_EMPTY);

    $intro = '';
    for ($i=0; $i<$n; $i++) {
           $intro .= $sentences[$i] . ($i < $n-1 ? ' ' : '');
    }

    return $intro;
}

/**
 * Content filter without shortcodes
 * @param  string $content        Post content
 * @param  string $allowable_tags Specify tags which should not be stripped
 * @return mixed                  Returns the filtered post content or null
 */
function fx_content_filters($content, $allowable_tags = '') {
    if (!$content) {
        return;
    }

    $content = strip_shortcodes($content);
    $content = apply_filters('the_content', $content);
    $content = strip_tags($content, $allowable_tags);

    return $content;
}

function fx_post_details() {
    get_template_part('loop/partials/post-details');
}

function fx_post_tags() {
    get_template_part('loop/partials/post-tags');
}

function fx_post_categories() {
    get_template_part('loop/partials/post-categories');
}

//
// Behind the scenes post functions
//

/**
 * Get post tags
 * @return string Returns all tags for a post with links
 */
function fx_get_post_tags() {
    global $post;

    $tags = wp_get_post_tags($post->ID);
    $return = '';

    foreach ($tags as $tag) {
        $tag_link = get_tag_link($tag->term_id);
        $tag_name = $tag->name;
        $return .= "<a href='$tag_link' title='$tag_name' class='tag'>$tag_name</a>";
    }
    return $return;
}

/**
 * Get post categories
 * @return string Returns all categories for a post with links
 */
function fx_get_post_categories() {
    global $post;

    $categories = get_the_category($post->ID);
    $result = '';
    $size = count($categories);
    $i = 1;

    foreach ($categories as $c) {
        $link = sprintf('<a href="%s">%s</a>', get_category_link($c->cat_ID), $c->name);
        $result .= $link . ($i < $size-1 ? ', ' : ($i === $size-1 ? ' &amp; ' : ''));
        $i++;
    }

    return $result;
}

/**
 * Pagination
 * @param  object $custom_query Optional custom query object
 * @return echo                 Echos the pagination links
 */
function fx_pagination($custom_query) {
    global $wp_query;
    $query = $custom_query ? $custom_query : $wp_query;
    $big = 999999999;
    $format = get_option('permalink_structure') ? '?paged=%#%' : 'page/%#%/';
    $base = $format == '?paged=%#%' ? str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ) : @add_query_arg('paged','%#%');

    echo paginate_links(array(
        'base' => $base,
        'format' => $format,
        'current' => max(1, get_query_var('paged')),
        'total' => $query->max_num_pages
    ));
}

/**
 * Get breadcrumb parent page
 * @param  string $post_type Post type in slug form
 * @return string            Returns parent page for post type as specified in theme options
 */
function fx_get_breadcrum_parent($post_type) {
    $theme_opt = $post_type . '_page';
    $page_id = get_opts($theme_opt);

    if ($page_id) {
        $permalink = get_permalink($page_id);
        return $permalink;
    }

    return '';
}

/**
 * Get breadcrumbs
 * @param  string  $home_text    Home text
 * @param  boolean $show_link    Show home crumb as link
 * @param  boolean $show_current Show current page as crumb
 * @return echo                  Returns the breadcrumbs for current post
 */
function fx_breadcrumbs($pipe = '/', $home_text = 'Home', $show_link = false, $show_current = true) {
    global $post;

    $delimiter = " $pipe ";
    $before = '<span>';
    $after = '</span>';

    $show_link = $show_link ? $show_link : get_site_url();
    $link_before = '<span typeof="v:Breadcrumb">';
    $link_after = '</span>';
    $link_attr = ' rel="v:url" property="v:title"';
    $link = $link_before . '<a' . $link_attr . ' href="%1$s">%2$s</a>' . $link_after;

    echo '<div id="crumbs" xmlns:v="http://rdf.data-vocabulary.org/#">';

    echo $home_text ? sprintf($link, $show_link, $home_text) . $delimiter : '';

    if (is_single() && get_post_type() === 'post') {
        // Single Post
        $post_type = get_post_type_object(get_post_type());
        $slug = $post_type->rewrite;
        $index_page_id = get_option('page_for_posts');
        $breadcrumb = sprintf($link, fx_get_breadcrum_parent('post'), get_the_title($index_page_id));

        echo $breadcrumb . $delimiter;

    } elseif (is_single() && get_post_type() === 'event') {
        $label = 'Race Calendar';
        $permalink = fx_get_breadcrum_parent(get_post_type());
        $breadcrumb = $permalink ? sprintf($link, $permalink, $label) : $label;

        echo $breadcrumb ? $breadcrumb . $delimiter : '';

    } elseif ( is_single() && !is_attachment()) {
        // Single CPT
        if (!$post->post_parent ) {
            $post_type = get_post_type_object(get_post_type());
            $label = $post_type->labels->breadcrumb;
            $permalink = fx_get_breadcrum_parent(get_post_type());
            $breadcrumb = $permalink ? sprintf($link, $permalink, $label) : $label;

            echo $breadcrumb ? $breadcrumb . $delimiter : '';

        } else {
            $parent_id  = $post->post_parent;
            $breadcrumbs = array();

            while ($parent_id) {
                $single_post = get_post($parent_id);
                $breadcrumbs[] = sprintf($link, get_permalink($single_post->ID), get_the_title($single_post->ID));
                $parent_id  = $single_post->post_parent;
            }

            $breadcrumbs = array_reverse($breadcrumbs);

            for ($i = 0; $i < count($breadcrumbs); $i++) {

                if ($i != count($breadcrumbs)-1) {
                    echo $delimiter;
                }
            }
        }

    } elseif (!is_single() && !is_page() && get_post_type() != 'post' && !is_404()) {
        // Some post type, not sure about this one

        $post_type = get_post_type_object(get_post_type());
        echo $before . $post_type->labels->singular_name . $after;

    } elseif (is_page() && !$post->post_parent) {
        // Single page, no parent

    } elseif (is_page() && $post->post_parent) {
        // Single page with parent
        $parent_id  = $post->post_parent;
        $breadcrumbs = array();

        while ($parent_id) {
            $page = get_page($parent_id);
            $breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
            $parent_id  = $page->post_parent;
        }

        $breadcrumbs = array_reverse($breadcrumbs);
        for ($i = 0; $i < count($breadcrumbs); $i++) {
            echo $breadcrumbs[$i];
            if ($i != count($breadcrumbs)-1) {
                echo $delimiter;
            }
        }
    }

    // Show current
    if ($show_current) {

        // Check if blog page
        if (is_home() && !is_front_page()) {
            $index_page_id = get_option('page_for_posts');
            echo $before . get_the_title($index_page_id) . $after;
        } else {
            echo $before . get_the_title() . $after;
        }
    }

    echo '</div>';
}
