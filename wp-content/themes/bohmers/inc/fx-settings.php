<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

/*------------------------------------*\
    Outgoing Email
\*------------------------------------*/

function from_mail($content_type) {
    return 'donotreply@domain.com.au';
}

add_filter('wp_mail_from','from_mail');

function from_name($name) {
    return 'Domain';
}

add_filter( 'wp_mail_from_name', 'from_name' );
