<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

/*------------------------------------*\
    Galleries
\*------------------------------------*/

function single_gallery_shortcode($atts) {
    extract( shortcode_atts( array(
        'id' => '',
    ), $atts ) );

    // $gallery_id needs to be accessible inside the component
    global $gallery_id;
    $gallery_id = $atts['id'];

    ob_start();
    get_template_part('components/gallery');
    return ob_get_clean();
}

add_shortcode( 'image-gallery', 'single_gallery_shortcode' );
