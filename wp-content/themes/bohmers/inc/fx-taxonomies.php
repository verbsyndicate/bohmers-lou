<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

// function add_custom_taxonomies() {
//     $labels = array(
//         'name'              => 'Categories',
//         'singular_name'     => 'Category',
//         'search_items'      => 'Search Categories'
//         'all_items'         => 'All Categories'
//         'parent_item'       => 'Parent Category'
//         'parent_item_colon' => 'Parent Category:
//         'edit_item'         => 'Edit Category'
//         'update_item'       => 'Update Category'
//         'add_new_item'      => 'Add New Category'
//         'new_item_name'     => 'New Category'
//         'menu_name'         => 'Categories'
//     );
//     $args = array(
//         'show_admin_column' => true,
//         'labels' => $labels,
//         'hierarchical' => true,
//     );
//     register_taxonomy( 'cpt_category', array('post_type'), $args );
// }
//
// add_action( 'init', 'add_custom_taxonomies', 0 );
