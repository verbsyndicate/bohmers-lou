<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

/*------------------------------------*\
    Theme Support
\*------------------------------------*/

if (function_exists('add_theme_support')) {
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 1200, '', true);
    add_image_size('medium', 700, '', true);
    add_image_size('small', 250, '', true);
}

/*------------------------------------*\
    Custom Login
\*------------------------------------*/

function custom_login_header() {
?>
    <style type="text/css">
        #login h1 a,
        .login h1 a {
            background: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo.png);
            background-repeat: no-repeat;
            background-size: 100%;
            background-position: center center;
            height: 120px;
            width: 320px;
            margin-bottom: 25px;
        }
    </style>
<?php }

function custom_url_login() {
    return get_site_url();
}

function custom_title_login() {
    return get_bloginfo('name');
}

add_action('login_enqueue_scripts', 'custom_login_header');
add_filter('login_headerurl', 'custom_url_login');
add_filter('login_headertitle', 'custom_title_login');
