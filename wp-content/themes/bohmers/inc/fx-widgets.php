<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

require_once('widgets/widget-contact-details.php');
require_once('widgets/widget-recent-posts-list.php');

function register_custom_widgets() {
    register_widget('ContactDetailsWidget');
    register_widget('RecentPostsListWidget');
}

add_action( 'widgets_init', 'register_custom_widgets' );
