<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

function get_current_post_id() {
    global $post;

    if ($post) {
        return $post->ID;
    }

    if (isset($_GET['post'])) :
        $post_id = $_GET['post'];
    elseif (isset( $_POST['post_ID'])) :
        $post_id = $_POST['post_ID'];
    else :
        $post_id = get_the_ID();
    endif;

    return $post_id;
}

function get_current_post() {
    global $post;

    if ($post) {
        return $post;
    }

    $post_id = get_current_post_id();

    return get_post($post_id);
}

function get_posts_children($parent_id = null){
    global $post;

    $post_parent = !empty($parent_id) ? $parent_id : $post->ID;
    $post_type = get_post_type($post_parent);

    $posts = get_posts(array(
        'numberposts' => -1,
        'post_status' => 'publish',
        'post_type' => $post_type,
        'post_parent' => $post_parent,
        'suppress_filters' => false
    ));

    return $posts;
}

function get_taxonomy_as_options($taxonomy, $use_slug = false, $hide_empty = false, $add_empty = false) {
    $options = array();
    $terms = get_terms($taxonomy, array(
        'hide_empty' => $hide_empty,
    ));

    if ($add_empty) {
        $options[] = array(
            'id' => '',
            'title' => 'All',
        );
    }

    foreach ($terms as $term) {
        $options[] = array(
            'id' => $use_slug ? $term->slug : $term->term_id,
            'title' => $term->name,
        );
    }

    return $options;
}

function is_child_post() {
    $post = get_current_post();

    return $post->post_parent > 0;
}

function is_home_page() {
    $post = get_current_post();

    return ('page-home.php' == get_post_meta($post->ID, '_wp_page_template', true));
}


function get_gallery_shortcode() {
    $post_id = get_current_post_id();

    return '[image-gallery id="' . $post_id . '"]';
}
