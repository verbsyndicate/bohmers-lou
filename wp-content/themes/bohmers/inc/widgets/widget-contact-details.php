<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

class ContactDetailsWidget extends CustomWidget {

    static $widget_id = 'contact_details_widget';
    static $options = array(
        'title' => 'Contact Details',
        'description' => 'Show contact details and social icons (optional).'
    );
    static $fields = array(
        'title' => array(
            'type' => 'textfield',
            'title' => 'Title:',
            'description' => ''
        ),
        'blurb' => array(
            'type' => 'textarea',
            'title' => 'Blurb:',
            'description' => ''
        ),
        'show_social' => array(
            'type' => 'checkbox',
            'title' => '',
            'label' => 'Show social icons',
            'description' => ''
        ),
    );

    public function widget( $args, $instance ) {

        $show_social = $instance['show_social'];
        $blurb = $instance['blurb'];

        echo $args['before_widget'];
        echo '<div class="contact-details-widget">';

        if (!empty($instance['title'])) {
            echo $args['before_title'] . esc_html( $instance['title'] ) . $args['after_title'];
        }

        $email = get_opts('email', true);
        $street_address = get_opts('street_address');
        $phone_numbers = json_decode(get_opts('phone_numbers', true));

        echo $blurb !== '' ? '<p class="contact-details__blurb">' . $blurb . '</p>' : '';

        echo '<ul class="contact-details__list">';

        if ($street_address) {
            echo '
                <li>
                    <span class="list-item__inner list-item__inner--icon"><span class="icon--map-marker"></span>' . $street_address . '</span>
                </li>
            ';
        }

        if ($phone_numbers && count($phone_numbers) > 0) {
            foreach ($phone_numbers as $number) {
                printf('
                <li>
                    <a class="list-item__inner list-item__inner--icon" href="tel:%2$s">
                        <strong>%1$s</strong>
                        <span class="icon--phone"></span>%2$s
                    </a>
                </li>
                ', $number->title, $number->value);
            }
        }

        if ($email) {
            echo '
                <li>
                    <a class="list-item__inner list-item__inner--icon" href="mailto:' .  $email . '" class="mail"><span class="icon--envelope"></span>' . $email . '</a>
                </li>
            ';
        }

        echo '</ul>';

        if ($show_social) {
            get_template_part('components/social-links');
        }

        echo '</div>';
        echo $args['after_widget'];
    }
}
