<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

class RecentPostsListWidget extends CustomWidget {

    static $widget_id = 'recent_posts_list_widget';
    static $options = array(
        'title' => 'Recent Posts (preview)',
        'description' => 'Show recent posts with preview.'
    );
    static $fields = array(
        'title' => array(
            'type' => 'textfield',
            'title' => 'Title:',
            'description' => 'Please enter a title for the widget.'
        ),
        'limit' => array(
            'type' => 'textfield',
            'input_type' => 'number',
            'title' => 'Number of Articles:',
            'default' => 4,
            'class' => 'meta-field--small'
        ),
        'show_blurb' => array(
            'type' => 'checkbox',
            'label' => 'Show blurb'
        )
    );

    public function widget( $args, $instance ) {
        $limit = $instance['limit'];
        $show_blurb = $instance['show_blurb'];

        $articles = get_posts(array(
            'post_type' => 'post',
            'posts_per_page' => $limit
        ));

        echo $args['before_widget'];
        echo '<div class="recent-posts-widget">';

        if (!empty($instance['title'])) {
            echo $args['before_title'] . esc_html( $instance['title'] ) . $args['after_title'];
        }

        foreach ($articles as $article) : setup_postdata($article);

            $id = $article->ID;
            $title = $article->post_title;

            echo '<div class="widget__article">';
            echo '<a href="' . get_permalink($id) .'">';
            echo '<div class="widget-article__thumbnail" style="' . fx_bg_style(false, $id, 'medium') . '""></div>';
            echo '<div class="widget-article__body">';
            echo '<h4 class="widget-article__title">' . $title . '</h4>';
            echo '<div class="widget-aritcle__details">';
            fx_post_details();
            echo '</div>';
            echo $show_blurb ? '<div class="widget-article__copy">' . fx_post_intro(2, $id) . ' </div>' : '';
            echo '</div>';
            echo '</a>';
            echo '</div>';

        endforeach; wp_reset_postdata();

        echo '</div>';
        echo $args['after_widget'];
    }
}
