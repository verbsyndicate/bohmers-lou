<?php get_header(); ?>
<?php
$index_page_id = get_option('page_for_posts');
$title = get_the_title($index_page_id);
$image_url = fx_post_image_url($index_page_id);
?>

<main role="main" class="main main--blog">
    <div class="main__header">
        <div class="header__wrap container">
            <h1 class="main__title">
                <span><?php echo $title; ?></span>
            </h1>
            <div class="main__breadcrumbs">
                <?php fx_breadcrumbs(); ?>
            </div>
        </div>
    </div>
    <div class="main__body main__body--has-sidebar">
        <div class="container">
            <div class="main__content">
                <div class="content__wrap">
                    <div class="row row--flex">
                        <?php
                        if (have_posts()) :
                            while (have_posts()) : the_post();

                                echo '<div class="col-md-6">';
                                get_template_part('loop/item-preview');
                                echo '</div>';

                            endwhile;
                        endif;
                        ?>
                    </div>
                    <?php get_template_part('components/pagination'); ?>
                </div>
            </div>

            <aside class="main__sidebar">
                <?php get_sidebar(); ?>
            </aside>
        </div>
    </div>
</main>


<?php get_footer(); ?>
