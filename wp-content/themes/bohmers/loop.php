<div class="loop-holder">
<?php if (have_posts()): while (have_posts()) : the_post(); ?>

<?php
if (is_search()) :
    get_template_part('loop/item-search');
else :
    get_template_part('loop/item-full');
endif;
?>

<?php endwhile; ?>

<?php else : ?>
    <?php get_template_part('loop/item-empty'); ?>
<?php endif; ?>
</div>
