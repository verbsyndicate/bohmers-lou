<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly
?>
<article class="loop__item">
    <h4 class="loop__title">Nothing was found</h4>
    <p>We couldn't find what you were looking for.<br />Maybe you could try searching?</p>
    <?php get_template_part('components/search-form'); ?>
</article>
