<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly
?>

<?php
$title = get_the_title();
$content = fx_post_blurb();
$permalink = get_permalink();
$image_bg_style = fx_bg_style(false, null, 'large');
?>

<article class="loop__item">
    <h4 class="loop__title">
        <?php echo $title; ?>
    </h4>
    <div class="loop__details">
        <?php fx_post_details(); ?>
    </div>
    <?php if ($image_bg_style) : ?>
        <div class="loop__image" style="<?php echo $image_bg_style; ?>"></div>
    <?php endif; ?>
    <?php if ($content) : ?>
        <div class="loop__blurb">
            <?php echo $content; ?>
        </div>
    <?php endif; ?>
    <a href="<?php echo $permalink; ?>" title="<?php echo $title; ?>" class="btn btn--border">Read more</a>
</article>
