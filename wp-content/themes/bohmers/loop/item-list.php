<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly
?>

<?php
$image_url = fx_post_image_url();
$title = get_the_title();
$content = fx_post_blurb();
$permalink = get_permalink();
?>

<article class="loop__item loop__item--list">
    <h4 class="loop__title">
        <?php echo $title; ?>
    </h4>
    <?php if ($content) : ?>
        <div class="loop__blurb">
            <?php echo $content; ?>
        </div>
    <?php endif; ?>
    <a href="<?php echo $permalink; ?>" title="<?php echo $title; ?>" class="btn btn--border">
        Read more
    </a>
</article>
