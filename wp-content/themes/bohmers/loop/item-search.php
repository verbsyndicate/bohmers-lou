<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly
?>

<?php
$title = get_the_title();
$content = fx_post_blurb();
$permalink = get_permalink();
$image_bg_style = fx_bg_style(false, null, 'large');
?>

<article class="loop__item loop__item--list loop__item--search">
    <a href="<?php echo $permalink; ?>" class="loop__link">
        <h4 class="loop__title">
            <?php echo $title; ?>
        </h4>
        <?php if ($image_bg_style) : ?>
            <div class="loop__blurb">
                <div class="loop__image loop__image--inline" style="<?php echo $image_bg_style; ?>"></div>
            </div>
        <?php endif; ?>
        <?php if ($content) : ?>
            <div class="loop__blurb">
                <?php echo $content; ?>
            </div>
        <?php endif; ?>
    </a>
</article>
