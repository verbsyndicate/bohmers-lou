<div class="post-nav">
<?php
    $prev_post = get_previous_post();
    $next_post = get_next_post();
?>
<?php if(!empty($prev_post)): ?>
    <a href="<?php echo get_permalink($prev_post->ID); ?>" class="prev" title="<?php echo $prev_post->ID; ?>"><span>&laquo;</span> Previous</a>
<?php endif; ?>
<?php if(!empty($next_post)): ?>
    <a href="<?php echo get_permalink($next_post->ID); ?>" class="next" title="<?php echo $next_post->ID; ?>">Next <span>&raquo;</span></a>
<?php endif; ?>
</div>
