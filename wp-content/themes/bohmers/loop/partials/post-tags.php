<?php

$tags = wp_get_post_tags($post->ID);

if (!empty($tags)) {

    echo '<div class="post__tags">';
    echo fx_get_post_tags();
    echo '</div>';

}
