<?php /*    Template Name: Contact Page    */ ?>

<?php get_header(); ?>

<?php

if (have_posts()) :
    while (have_posts()) : the_post();
        $image_url = fx_post_image_url();
        $subheading = fx_get_meta('subheading');
?>

<main role="main" class="main">
    <div class="main__header" style="<?php fx_bg_style(); ?>">
        <div class="header__wrap container">
            <h1 class="main__title">
                <span><?php the_title(); ?></span>
            </h1>
            <?php if ($subheading !== '') : ?>
                <h4 class="main__subtitle">
                    <?php echo $subheading; ?>
                </h4>
            <?php endif; ?>
            <div class="main__breadcrumbs">
                <?php fx_breadcrumbs(); ?>
            </div>
        </div>
    </div>
    <div class="main__body">
        <div class="container">
            <div class="main__content">
                <div class="content__wrap">
                  <div class="row">
                    <div class="col-md-8 col-sm-6 col-xs-12">
                      <?php
                          the_content();
                      ?>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                      <div class="contact__page_details_container">
                        <?php
                        $phone_numbers = json_decode(get_opts('phone_numbers', true));
                        if ($phone_numbers && count($phone_numbers) > 0) {
                            foreach ($phone_numbers as $number) {
                                printf('
                                <div>
                                    <strong>%s</strong><br />
                                    <span class="icon--phone">%s</span>
                                </div>
                                ', $number->title, $number->value);
                            }
                        }
                        ?>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php
    endwhile;
endif;
?>

<?php get_footer(); ?>
