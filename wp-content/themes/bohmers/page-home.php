<?php /*
	Template Name: Home Page
*/ ?>

<?php get_header(); ?>

<?php
if (have_posts()) :
    while (have_posts()) : the_post();
?>

<div class="home-background" style="<?php fx_bg_style(); ?>">
</div>
<div class="home-video__fallback" style="<?php fx_bg_style(); ?>"></div>
<div class="home-bg-video">
  <video width="100%" height="100%" autoplay muted loop>
    <!-- <source src="<?php theme_dir(); ?>/assets/video/20170302_BOHMERSTEXTUREPACKAGE.mp4" type="video/mp4"> -->
    <source src="<?php theme_dir(); ?>/assets/video/20170302_BOHMERSTEXTUREPACKAGE.webm" type="video/webm">
    <!-- <source src="movie.ogg" type="video/ogg"> -->
  </video>
</div>
<div class="home-hero" id="home-hero">
  <div class="home-hero__logo-wrapper">
    <img class="home-hero__logo" src="<?php theme_dir(); ?>/assets/img/bohmers_logo_white-text-tm.png" alt="Bohmer's Tree Care" />
    <div class="home-hero__safework_container">
      <img class="home-hero__safework-logo" src="<?php theme_dir(); ?>/assets/img/safework_awards_2016_winner.png"/>
    </div>
  </div>
</div>

<?php get_template_part('components/home-services'); ?>

<?php get_template_part('components/home-map'); ?>

<?php get_template_part('components/home-clients'); ?>

<?php get_template_part('components/home-testimonials'); ?>

<?php get_template_part('components/home-recent-posts'); ?>

<?php get_template_part('components/home-signup-mailchimp'); ?>
<?php
    endwhile;
endif;
?>

<?php get_footer(); ?>
