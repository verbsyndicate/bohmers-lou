<?php get_header(); ?>

<?php
if (have_posts()) :
    while (have_posts()) : the_post();
        $image_url = fx_post_image_url();
        $subheading = fx_get_meta('subheading');
?>

<main role="main" class="main <?php get_the_slug ()?>">
    <div class="main__header" style="<?php fx_bg_style(); ?>">
        <div class="header__wrap container">
            <h1 class="main__title"><span><?php the_title(); ?></span></h1>
            <?php if ($subheading !== '') : ?>
                <h4 class="main__subtitle">
                    <?php echo $subheading; ?>
                </h4>
            <?php endif; ?>
            <div class="main__breadcrumbs">
                <?php fx_breadcrumbs(); ?>
            </div>
        </div>
    </div>
    <div class="main__body container">
        <div class="main__content">
            <div class="content__wrap">
                <?php the_content(); ?>
            </div>
        </div>
    </div>
</main>

<?php
    endwhile;
endif;
?>

<?php get_footer(); ?>
