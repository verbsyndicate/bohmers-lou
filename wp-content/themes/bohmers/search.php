<?php get_header(); ?>

<main role="main" class="main">
    <div class="main__header">
        <div class="header__wrap container">
            <h1 class="main__title">Search Results</h1>
            <div class="main__subheading">
                <h4 class="main__subtitle">
                    We found <?php echo $wp_query->found_posts . ' result' . ($wp_query->found_posts != 1 ? 's' : '') . ' for your search <em>"' . get_search_query() . '"</em>'; ?>
                </h4>
            </div>
        </div>
    </div>
    <div class="main__body main__body--has-sidebar">
        <div class="container">
            <div class="main__content">
                <?php get_template_part('loop'); ?>
                <?php get_template_part('components/pagination'); ?>
            </div>

            <aside class="main__sidebar">
                <?php get_sidebar(); ?>
            </aside>
        </div>
    </div>
</main>

<?php get_footer(); ?>
