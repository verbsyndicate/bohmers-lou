<?php get_header(); ?>

<?php
if (have_posts()) :
    while (have_posts()) : the_post();
        $image_url = fx_post_image_url();
?>

<main role="main" class="main main--single-post">
    <div class="main__header" style="<?php fx_bg_style(); ?>">
        <div class="header__wrap container">
            <h1 class="main__title"><span><?php the_title(); ?></span></h1>
            <div class="main__breadcrumbs">
                <?php fx_breadcrumbs(); ?>
            </div>
        </div>
    </div>
    <div class="main__body main__body--has-sidebar">
        <div class="container">
            <div class="main__content">
                <div class="content__wrap">
                    <?php the_content(); ?>
                </div>

                <?php fx_post_tags(); ?>

            </div>
            <aside class="main__sidebar">
                <?php get_sidebar(); ?>
            </aside>
        </div>
    </div>
</main>

<?php
    endwhile;
endif;
?>

<?php get_footer(); ?>
